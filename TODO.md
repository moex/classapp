# TODO list

## CLI
- Classification-based guesser strategy
- Knower strategy: put one card by minimising the information of that card (on patterns)
- More strategies:
  - probabilistic
  - constraint-based
  - neural network learned
- Add a class for running automatic games and collecting statistics (testing benefits of strategies, training learners...).
- To replay (currently some code is here for storing and replaying games but this is not fully implemented):
  - reload classif (easy)
  - reload deck! (this is only the seed)
  - reload initial service

## GUI
- Implement change of parameters useful for jar-launched
- add scrollers on all info panels (too small on windows)
- Make the game so that only the knower has the classification (not difficult)
- Guess classification
- Make card and classes reorganisable
- Add a counter for (a) counting the player score (remaining cards) and (b) displaying that of all players.
- Terrible colors (?)

## Whisper version (CLI)
- Generate initial classification / Create table from random (2*nbleaves) classes / Infer classification / Continue
- Develop it in population mode
- Develop factorial plan

## Misc
- Networked, multi-players server?
- Making it browser/smartphone app?
- Extending it to all games of the _Class?_ curriculum
  - Write a program that: Given a board; Allow user to ask some questions (is this a class); Guess the classification
- Add licenses (should be GPL 2.1 / CeCCIL-?)

## NOTES

### SVG Library (2022-08-13)

Cards are rendered from SVG. The ClassCard class has been implemented with three SVG libraries with the aim of reducing the application footprint:
* batik (1.14, 4MB, works)
* jsvg (0.3.0, 300KB, works but slightly less neat than batik)
* SVG Salamander (1.1.3, 312KB, does not print well fillings)

The three implementations are available in the ClassCard code, but only batik is used (and only its jars loaded).

The relatively large footprint in the classgui Jar seems anyway due to our svg cards!
Even if it is only text, it is clear that it could largely be rationalised.

/*
 * Copyright (C) INRIA, 2021-2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Properties;

import java.io.PrintWriter;
import java.io.File;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractClassPlayer implements ClassPlayer {
    final static Logger logger = LoggerFactory.getLogger( AbstractClassPlayer.class );

    public ClassEnvironment env = null;
    public int envid = 0;
    // Eventually, give it a name (default "Agent #envid")

    protected Properties parameters;

    protected boolean memory = false;  // do agents memorise what has happened

    protected boolean hints = false;  // does the system prints hints!

    protected static int NBFEATURES;
    protected static int NBCARDS;

     /**
     * Where (and if) to save data to
     */
    // Should be fed by ClassSession
    // Set to null if no print
    protected PrintStream os = System.out;

    private File initDir = null;
    private File finalDir = null;

    // these two variables change...
    CardSet cards;

    int nbClasses = 0;

    // JE2022: TODO This may be deprecated
    // Or implement the interactive strategy here?
    public void setInteractive() {
	logger.error( "IGNORED: Interactive strategy not implemented" );
    }
    
    public boolean isInteractive() {
	return false;
    }
    
    // It is now fully disconnected from the ontology URI which is given otherwise
    public AbstractClassPlayer( ClassEnvironment e, int id ) { // To be generalised
	env = e;
	envid = id;
	logger.debug( PlayClass.getString( "i6" ), id );
    }

    public ClassPlayer init( Properties param ) {
	parameters = param;
	NBCARDS = CardSet.NBCARDS;
	NBFEATURES = Feature.NBFEATURES;
	cards = new CardSet();
	if ( param.getProperty( "hints" ) != null ) {
	    hints = true;
	}
	if ( param.getProperty( "initDir" ) != null ) { // Ugly
	    initDir = new File( param.getProperty( "initDir" )+"/agent", ""+envid );
	}
	if ( param.getProperty( "finalDir" ) != null ) {
	    finalDir = new File( param.getProperty( "finalDir" )+"/agent", ""+envid );
	}
	if ( param.getProperty( "memory" ) != null ) {
	    memory = true;
	}

	//logger.debug( PlayClass.getString( "i7" ), envid );
	return this;
    }

    public int getId() { return envid; }

    public int nbCards() {
	// Would be better with set interface...
	return cards.cardinality();
    }

    /**
     * These are here for facilitating the API
     **/
    public CardSet getHand() {
	return cards;
    }

    public int getCurrentClassNumber() {
	return env.nbClasses();
    }
        
    public CardSet getCurrentClassCards( int cl ) {
	return env.getCards( cl );
    }
        
    /**
     * TO IMPLEMENT
     **/

    public abstract void acceptNotice( ClassGame game );

    public void receiveCards( CardSet crds ) {
	// Add them to cards
	cards.addCards( crds );
	logger.debug( PlayClass.getString( "i13" ), envid, knower?"*":"", crds.toPatternList() );
    }

    public void releaseCards( CardSet crds ) {
	// Suppress cards from own lot
	cards.removeCards( crds );
	// return these
    }
    
    public abstract void playGame( ClassGame game ) throws Exception;

    /***
     * THIS IS NOT PART OF THE INTERFACE
     **/
    // So stupid! they have the same violations
    private boolean sameViolations ( int v1, int v2 ) {
	return ( v1 == v2 );
    }
    
    /**
     * For each value of the card, 
     * If the value of the class of the same or *, then it is safe
     * otherwise it is not
     * This returns a table: class \times cards containing
     * the incompatibility pattern (see incompatibility below)
     * Beware, the table contains 81 cards...
    */
    protected int[][] getIncompatibility () {
	return env.getIncompatibility( cards );
    }

    protected void printHand( PrintStream os ) {
	os.print( PlayClass.getString( "i15" ) );
	for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
	    os.print( "\t"+Card.toPattern( i ) );
	}
	os.println();
    }    
    
    protected void printHints( PrintStream os ) {
	printIncompatibility( getIncompatibility(), os );
    }
    
    private void printIncompatibility( int[][] inc, PrintStream os ) {
	os.print( "\t" );
	for ( int cl = 0; cl < nbClasses; cl++ ) {
	    int[] samediff = env.getSameDiff( cl );
	    os.print( cl+"="+printClass( samediff ) );
	    os.print( "\t" );
	}
	for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
	    os.println();
	    os.print( Card.toPattern( i )+"\t" );
	    for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
		// memory
		CardSet wrong = env.getWrongs( cl );
		if ( wrong.get( i ) ) os.print( "! " ); else os.print( "  " ); 
		printIncPattern( inc[cl][i], os );
		os.print( "\t" );
	    }
	}
	os.println();
    }

    protected int nbInc( int inc ) {
	int result = 0;
	if ( inc>=8 ) result++;
	if ( inc%8>=4 ) result++;
	if ( inc%4>=2 ) result++;
	if ( inc%2>=1 ) result++;
	return result;	
    }
    
    private void printIncPattern( int inc, PrintStream os ) {
	//os.print( inc+"/" );
	if ( inc>=8 ) os.print("?"); else os.print("_");
	if ( inc%8>=4 ) os.print("?"); else os.print("_");
	if ( inc%4>=2 ) os.print("?"); else os.print("_");
	if ( inc%2>=1 ) os.print("?"); else os.print("_");
    }
    
    private String printClass( int[] SD ) {
	String result = "";
	for ( int i=0; i < NBFEATURES ; i++ ) {
	    if ( SD[i] == -1 ) {
		result += "*";
	    } else {
		result += SD[i];
	    }
	}
	return result;
    }
    
    // JE2022: cancelled for genericity
    // Add to the interface?
    public void displayCards() {
	logger.debug( PlayClass.getString( "i13b" ), envid, knower?"*":"", cards.toPatternList() );
    }

    /**
     * EXTRA FUNCTION
     **/
    // Use our compact notation?
    public void guessClassification() {}

    // When Knower
    protected boolean knower = false;
    protected Classification classification;

    public void setKnower( Classification classif ) {
	classification = classif;
	knower = true;
    }
    
    public boolean isKnower() {
	return knower;
    }
    
    public boolean judgeTurn( ClassGame game ) throws Exception {
	if ( !knower ) throw new Exception( "Only the knower can judge a turn" );
	// Forall cards, are they attached to the given class or none
	if ( game.getAssigned() == -1 ) {
	    CardSet bscl = env.isAClass( game.getCards() );
	    return ( bscl != null );
	} else {
	    return env.getLeave( game.getAssigned() ).includes( game.getCards() );
	}
    }
    
    // Does nothing?
    public void save( File dir ) throws Exception {
	PrintWriter out = new PrintWriter( new File( dir, "sig.txt" ) );
    }

    public void load( File dir ) throws Exception {
    }
}

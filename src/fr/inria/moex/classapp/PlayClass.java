/*
 *
 * Copyright (C) INRIA, 2021-2022, 2025
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Properties;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Locale;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import java.lang.reflect.Constructor;

/* liekly */
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayClass {
    final static Logger logger = LoggerFactory.getLogger( PlayClass.class );

    /** Dealing with multilinguism.
		This will not work util Java 8 if the encoding is UTF-8
		The best soluton (besides using at least Java 11) is given by
		https://stackoverflow.com/a/37039868/7703978
    **/
    private static ResourceBundle bundle;
    
    private static Locale loc;

    public static void updateLocale() {
		if ( loc == null ) loc = Locale.getDefault();
		bundle = ResourceBundle.getBundle( "fr.inria.moex.classapp.resources.strings", loc );
    }
    
    public static String getString( final String key ) {
        if ( bundle == null ) updateLocale();
		String str = bundle.getString(key);
		if ( str != null ) return str;
		else return "___";
    }    

    public static String getHTMLFile( final Class cl, final String dir, final String filepref ) {
		if ( loc == null ) loc = Locale.getDefault();
		try {
			return getFile( cl, "/fr/inria/moex/classgui/resources/", filepref+"_"+loc.getLanguage()+".html" );
		} catch ( Exception ex1 ) {
			try {
				return getFile( cl, "/fr/inria/moex/classgui/resources/", filepref+".html" );
			} catch ( Exception ex2 ) {
				return ex2.toString();
			}
		}
    }
	
    public static String getFile( final Class cl, final String dir, final String filename ) throws IOException {
		String content = "";
		InputStream instr = cl.getResourceAsStream( dir+filename );
		InputStreamReader strrd = new InputStreamReader( instr, StandardCharsets.UTF_8 );
		BufferedReader rr = new BufferedReader(strrd);
		String line;
		while ((line = rr.readLine()) != null) 
			content += line;
		return content;
    }    
	
    public static Locale getLocale() {
        return loc;
    }    

    public static void setLocale( final Locale locale ) {
        loc = locale;
		updateLocale();
    }    

    /** From commonCli **/
    protected Options options = null;
    
    protected String outputfilename = null;

    protected Properties parameters = null;

    /**
     * The number of sessions to be run (-DnbRuns=)
     */
    private int nbRuns = 5;

    // Reuses CommonCLI, create our own if needed
    public PlayClass() {
		parameters = new Properties();
		setUpOptions();
    }

    protected void setUpOptions() {
		options = new Options();
		Option opt = null;
		// h = no arg
		//options.addOption( createOption( "h", "help", "Print this page" ) );
		opt = new Option( "h", "Print this page" );
		opt.setLongOpt( "help" );
		options.addOption( opt );
		// o = req
		//options.addOption( createRequiredOption( "o", "output", "Send output to FILE", "FILE" ) );
		opt = new Option( "o", "Send output to FILE" );
		opt.setLongOpt(  "output" );
		opt.setArgs( 1 );
		opt.setArgName( "FILE" );
		options.addOption( opt );
		// P = req
		//options.addOption( createRequiredOption( "P", "params", "Read parameters from FILE", "FILE" ) );
		opt = new Option( "P", "Read parameters from FILE" );
		opt.setLongOpt(  "params" );
		opt.setArgs( 1 );
		opt.setArgName( "FILE" );
		options.addOption( opt );
		// D special parameter
		opt = new Option( "D", "Use value for given property" );
		opt.setArgs(2);
		opt.setValueSeparator('=');
		opt.setArgName( "NAME=VALUE" );
		options.addOption( opt );
    }
    
    File saveDir = null;
    File loadDir = null;

    // In spirit, this is final
    public CommandLine parseCommandLine( String[] args ) throws ParseException {
		CommandLineParser parser = new DefaultParser();
		CommandLine line = parser.parse( options, args );
		parameters = line.getOptionProperties( "D" );
		if ( line.hasOption( 'd' ) ) {
			logger.warn( "debug command-line switch DEPRECATED, use logging" );
		}
		if ( line.hasOption( 'o' ) ) {
			outputfilename = line.getOptionValue( 'o' );
		}
		if ( line.hasOption( 'P' ) ) {
			try ( FileInputStream fis = new FileInputStream( line.getOptionValue( 'P' ) ) ) {
				parameters.loadFromXML( fis );
			} catch (FileNotFoundException fnfex) {
				logger.warn( "File {} not found", line.getOptionValue( 'P' ), fnfex );
				//throw new ParseException("There isn't such file: " + line.getOptionValue( 'P' ), fnfex);
			} catch (IOException ioex) {
				logger.warn( "Cannot parse parameter file {}", line.getOptionValue( 'P' ), ioex );
				//throw new ParseException("Error accessing file " + line.getOptionValue( 'P' ), ioex);
			}
		}
		if ( line.hasOption( 'h' ) ) {
			usage(0);
		}
		return line;
    }
	
    // This one has a main()
    public static void main( String[] args ) {
		try { new PlayClass().run( args ); }
		catch ( Exception ex ) {
			ex.printStackTrace();
			System.exit(-1);
		};
    }

    // 2020: split into parse+init+process+report
    // It is unclear if all the treatment
    public void run( String[] args ) throws Exception {
		CommandLine line = null;
		logger.debug( getString( "i1" ) );
		try { 
			line = parseCommandLine( args );
			if ( line == null ) System.exit(1); // or return
			// Here deal with command specific arguments
			for ( Object o : line.getArgList() ) {
				logger.info( getString( "i2a" ), o );
			}
			for ( Entry<Object,Object> m : parameters.entrySet() ) {
				logger.info( getString( "i2p" ), m.getKey(), m.getValue() );
			}
		} catch( ParseException exp ) {
			logger.error( exp.getMessage() );
			usage();
			System.exit(-1);
		}
		logger.debug( getString( "i3" ) );
		// Initialize (already done here with the parsed parameters)
		init( parameters );
		// Run session
		process();
    }
	
    public void init( Properties params ) throws IOException, FileNotFoundException {
		if ( parameters != params ) parameters = params; // called from outside
		// Get load directory
		if ( parameters.getProperty( "loadDir" ) != null ) {
			loadDir = new File( parameters.getProperty( "loadDir" ) );
			if ( parameters.getProperty( "loadParams" ) != null ) {
				// load them from file
				Properties storedParams = new Properties();
				storedParams.loadFromXML( new FileInputStream( new File( loadDir, "params.xml" ) ) );
				// == add new params... but loadParams??? to loaded ones?
				storedParams.list( System.err );
				// merge both parameter sets... (maybe not savedir)
				storedParams.forEach( (k, v) ->
									  { if ( !((String)k).equals("saveDir") )
											  parameters.setProperty( (String)k, (String)v ); });
				//parameters.list( System.err );
			}
		}
		// Get save directory
		if ( parameters.getProperty( "saveDir" ) != null ) {
			saveDir = new File( parameters.getProperty( "saveDir" ) );
			saveDir.mkdir();
			if ( parameters.getProperty( "saveParams" ) != null ) {
				//store the properties detail into a pre-defined XML file
				parameters.storeToXML( new FileOutputStream( new File( saveDir, "params.xml" ) ),
									   "Lazy lavender session parameters", "UTF-8");
			}
		}
		// Get nbRuns
		if ( parameters.getProperty( "nbRuns" ) != null ) {
			nbRuns = Integer.parseInt( parameters.getProperty( "nbRuns" ) );
		}
    }
    
    public void process() throws Exception {
		// The real work
		for ( int i = nbRuns-1; i >= 0; i-- ) {
			logger.debug( getString( "i4" ), i );
			if ( loadDir != null ) {
				parameters.setProperty( "loadRunDir", loadDir.toString()+"/"+i );
			}
			ClassSession session = new ClassSession();
			
			// create save directory
			if ( saveDir != null ) {
				parameters.setProperty( "runDir", saveDir.toString()+"/"+i ); // could be better...
			}
			session.init( parameters );
			session.initSession();
			logger.debug( getString( "i9" ), i );
			session.process();
		}
		return;
    }
    
    public void exit( int returnCode ) {
		System.exit( returnCode );
    }

    public void usage( int returnValue ) {
		usage();
		System.exit( returnValue );
    }

    public void usage() {
		new HelpFormatter().printHelp( 80, "java "+getClass().getName()+" [options]", "\nOptions:", options, "\nSee https://gforge.inria.fr/plugins/mediawiki/wiki/lazylav/index.php/Parameters for an extensive list of properties\n" );
    }
}

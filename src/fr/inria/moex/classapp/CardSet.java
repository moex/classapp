/*
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

/**
 * The implementation of CardSet is:
 * - an extension of BitSet (that can be manipulated as such in internals)
 * - a clean API that can be exploited as interface from outside
 */

package fr.inria.moex.classapp;

import java.util.BitSet;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CardSet extends BitSet implements Iterable<Card> {

    final static int NBCARDS = (int)Math.pow( Card.NBVALUES, Feature.NBFEATURES );

	private static final long serialVersionUID = 7575757575757575757L;	

    public CardSet () {
		super( NBCARDS ); // initialised at empty
    }

    public boolean includes( Card card ) {
		return get( card.getRepr() );
    }
    public boolean includes( int c ) {
		return get( c );
    }
    // Test what is the faster anyway!
    public boolean includes( CardSet cards ) {
		// Alternative, but slower (after benchmarks), implementation
		//CardSet test = (CardSet)(cards.clone());
		//test.andNot( this );
		// return test.isEmpty();
		for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
			if ( !get( i ) ) return false;
		}
		return true;
    }

    public void addCard( int c ) {
		set( c, true );
    }
    public void addCard( Card card ) {
		set( card.getRepr(), true );
    }
    public void addCards( CardSet cards ) {
		or( cards );
    }

    public void removeCard( int c ) {
		set( c, false );
    }
    public void removeCard( Card card ) {
		set( card.getRepr(), false );
    }
    public void removeCards( CardSet cards ) {
		andNot( cards );
    }

    public int size() {
		return cardinality();
    }

    public Iterator<Card> iterator() {
		return new CardSetIterator( this );
    }    

	/**
	 * returns the pattern (imposed values/-1) that correspond to a set of cards
	 */
    public int[] pattern() {
		if ( isEmpty() ) return null; // The empty set has no pattern, test before!
		int[] result = Card.featureValues( nextSetBit( 0 ) );
		for ( Card card : this ) {
			int c = card.getRepr();
			for ( int i = 0; i < Feature.NBFEATURES; i++ ) {
				if ( result[i] != Card.featureValue( c, i )  ) result[i] = -1;
			}
		}
		return result;
    }    

    public String toPatternList() {
		String result = "{";
		for ( int i=0; i < NBCARDS; i++ ) {
			if ( get( i ) ) {
				result += " "+Card.toPattern( i );
				/*
				  result += " ";
				  result += (number+1);
				  result += "-";
				  switch (filling) {
				  case 0: result += "full"; break;
				  case 1: result += "hatched"; break;
				  case 2: result += "empty"; break;
				  }
				  result += "-";
				  switch (color) {
				  case 0: result += "red"; break;
				  case 1: result += "green"; break;
				  case 2: result += "blue"; break;
				  }
				  result += "-";
				  switch (shape) {
				  case 0: result += "circle"; break;
				  case 1: result += "triangle"; break;
				  case 2: result += "square"; break;
				  }
				*/
			}
		}
		return result+" }";
    }
}

/**
 * Specific BitSet iterator for Cards
 **/
class CardSetIterator implements Iterator<Card> {
	
    private int currentIndex = 0;
    private CardSet bitset;
	
    CardSetIterator( CardSet bs ){
		bitset = bs;
		currentIndex = bitset.nextSetBit(0);
    }
    public boolean hasNext(){
		return ( currentIndex >= 0 ); // != -1 );
    }
    public Card next() {
		Card card = Card.getCard( currentIndex );
		currentIndex = bitset.nextSetBit( currentIndex+1 );
		return card;
    }
}

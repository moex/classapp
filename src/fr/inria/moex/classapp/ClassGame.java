/*
 * Copyright (C) INRIA, 2021-2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

/**
 * A turn of the game.
 * Which agent plays
 * What cards,
 * Where
 */
public class ClassGame { 
    final static Logger logger = LoggerFactory.getLogger( ClassGame.class );

    // Player
    ClassPlayer player;
    // List of card
    CardSet cards;
    // Where assigned (-1=new class)
    int assignment = -1;
    // Issue (correct or incorrect)
    boolean issue = false;
    
    public ClassGame( ClassPlayer a ) {
	player = a;
    }

    public ClassGame( ClassPlayer a, CardSet crds, int cl ) {
	this( a );
	play( crds, cl );
    }

    public void play( CardSet crds, int cl ) {
	cards = crds;
	assignment = cl;
    }

    public void setCards( CardSet c ) {
	cards = c;
    }

    public boolean newClass() {
	return ( assignment == -1 );
    }

    public CardSet getCards() {
	return cards;
    }

    public int getAssigned() {
	return assignment;
    }

    public ClassPlayer getPlayer() {
	return player;
    }

    public int getPlayerId() {
	return player.getId();
    }

    public boolean getIssue() {
	return issue;
    }

    public void setIssue( boolean i ) {
	issue = i;
    }

    public void save( PrintWriter file ) throws Exception {
	// does nothing so far...
    }

}

/*
 * Copyright (C) INRIA, 2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

/**
 * A tentative cards interface...
 * Card: basically an integer representing the card
 */

package fr.inria.moex.classapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Card {

    public static int NBFEATURES = Feature.NBFEATURES;
    public static int NBVALUES = 3;

    public Card() {}

    public Card( Feature.Number number, Feature.Shape shape, Feature.Filling filling, Feature.Color color ) {
		this();
		card = 0;
		card += featureEncoding( Feature.NUMBER.ordinal(), number.ordinal() );
		card += featureEncoding( Feature.SHAPE.ordinal(), shape.ordinal() );
		card += featureEncoding( Feature.FILLING.ordinal(), filling.ordinal() );
		card += featureEncoding( Feature.COLOR.ordinal(), color.ordinal() );
    }

    /**
     * Computes at initialisation the power of features
     */    
    //powers[i]=NBVALUES^(NBFEATURES-i+1)
    // 0: p^4, 1: p^3, 2: p^2, 3: p
    
    private static int[] powers;
	
    private static int getPower( int feature ) {
		if ( powers == null ) initPowers(); // Can't we do this at load time?
		return powers[feature];
    }
    
    private static void initPowers () {
		powers = new int[NBFEATURES];
		int p = Card.NBVALUES;
		for ( int i=NBFEATURES-1; i >= 0; i-- ) {
			powers[i] = p;
			p *= NBVALUES;
		}
    }

    public static int featureValue( int card, int feature ) {
		if ( powers == null ) initPowers(); // Can't we do this at load time?
		if ( feature == NBFEATURES-1 ) return card%(powers[feature]);
		else if ( feature == 0 ) return card/powers[feature+1];
		else return (card%powers[feature])/powers[feature+1];
    }
	
    public int[] featureValues() {
		return featureValues( card );
	}
	
    public static int[] featureValues( int card ) {
		if ( powers == null ) initPowers(); // Can't we do this at load time?
		int[] result = new int[NBFEATURES];
		for ( int feature = 0; feature < NBFEATURES; feature++ ) {
			// (card%powers[feature])/powers[feature+1];
			// DO BETTER!
			result[feature] = featureValue( card, feature );
		}
		return result;
    }
	
    public static int featureEncoding( int feature, int value ) {
		if ( powers == null ) initPowers(); // Can't we do this at load time?
		if ( feature == NBFEATURES-1 ) return value;
		else return powers[feature+1]*value;
    }

    /**
     * internals
     * Cards which in the genuine implementation do not have to exist
     * are here cached for the API (CardSet iterator)
     **/
    private int card;

    private static Card[] cardCache;

    private static void initCache() {
		cardCache = new Card[CardSet.NBCARDS];
    }

    private Card( int i ) {
		card = i;
		putCard( i, this );
    }
    private void putCard( int i, Card crd ) {
		if ( cardCache == null ) initCache();
		if ( cardCache[i] == null ) cardCache[i] = crd;
    }
    public static Card getCard( int i ) {
		if ( cardCache == null ) initCache();
		if ( cardCache[i] == null ) {
			return new Card( i ); // it is put in cache
		} else {
			return cardCache[i];
		}
    }
	
	public int getRepr() { return card; }
	
    public static int NUMBER = 0;
    public static int FILLING = 1;
    public static int SHAPE = 2;
    public static int COLOR = 3;

    public Feature.Number getNumber() {
		return Feature.NUMBERS_INDEX[featureValue( card, NUMBER )];
    }
    
    public Feature.Filling getFilling() {
		return Feature.FILLINGS_INDEX[featureValue( card, FILLING )];
    }
    
    public Feature.Shape getShape() {
		return Feature.SHAPES_INDEX[featureValue( card, SHAPE )];
    }
    
    public Feature.Color getColor() {
		return Feature.COLORS_INDEX[featureValue( card, COLOR )];
		//return Feature.COLORS_INDEX[1];
    }
    
    public static String toPattern( int c ) {
		String result = "";
		for ( int i=0; i<NBFEATURES; i++ ) {
			result += featureValue( c, i );
		}
		return result;
    }
    public String toPattern() {
		return toPattern( card );
    }
    
    public static int patternToInt( String pattern ) {
		int result = 0;
		if ( pattern.length() != NBFEATURES ) return -1;
		for ( int i=0; i<NBFEATURES; i++ ) {
			int c = pattern.charAt( i ) - '0';
			// also test that not sup
			if ( ! ( c >= 0 ) ) return -1;
			result += featureEncoding( i, c );
		}
		return result;
    }
	
}

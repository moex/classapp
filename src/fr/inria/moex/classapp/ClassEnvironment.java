/*
 * Copyright (C) INRIA, 2021-2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.io.File;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class defines 
 * The GenBoard and Classification part are taken from GenBoard (classgame)
 */

public class ClassEnvironment {
	final static Logger logger = LoggerFactory.getLogger( ClassEnvironment.class );

	protected static int NBFEATURES = Feature.NBFEATURES;
	protected static int NBCARDS = CardSet.NBCARDS;

	public static int getNbCards() {
		return NBCARDS;
	}

	/**
	 * Classification / Board
	 */
	Classification board;

	/**
	 * Deck
	 */
	Deck deck;

	public int drawCard() throws Exception {
		return deck.drawCard();
	}

	public CardSet drawCards( int nb ) throws Exception {
		return deck.drawCards( nb );
	}

	/**
	 * Number of levels in classification (-DnbLevels)
	 */
	private int NBLEVELS = 3;

	public int numberOfLevels() {
		return NBLEVELS;
	}

	/**
	 * Number of leaves in classification (-DnbLeaves)
	 */
	private int NBLEAVES = 9;

	public int numberOfLeaves() {
		return NBLEAVES;
	}

	public Classification genBoard( int levels, int leaves, boolean exact ) throws Exception {
		board = Classification.genBoard( levels, leaves, exact );
		return board;
	}

	public Classification genBoard() throws Exception {
		board = Classification.genBoard( NBLEVELS, NBLEAVES, true );
		return board;
	}

	/**
	 * Table: the set of classes featured on the table
	 */

	private int nbClasses = 0;
	// each position in these vectors correspond to a class on the table
	// the card set is the set of cards on the table
	private Vector<CardSet> classes;
	// the card set is the set of cards corresponding to the leaf represented by this class
	private Vector<CardSet> leaves;

	public CardSet getLeave( int i ) {
		return leaves.get( i );
	}

	// the int[] are class patterns generalised from the set of classes
	// one index per feature, -1 means the feature is indifferent, 0..NBVALUES-1 to corresponding values
	private Vector<int[]> samediff;
	/**
	 * Memory.
	 * - fullmem: store all hands
	 * - simplemem: store one 
	 * - clevermem: simplemem + propagate (conflicts learned)
	 * - this propagation may also be made with fullmem
	 */

	// the bitset is the set of hands that have been wrongly played (this is a non complete memory (which should be Vector<Set<CardSet>>
	Vector<CardSet> wrong;

	public int nbClasses() {
		return nbClasses;
	}

	/**
	 * Assigns a set of cards to a particular class
	 * A class is a card set refering to all the cards it has on the table
	 * In the leave vector is the corresponding leaf class (as bit set as well!)
	 */ 
	public void assignCards( int cl, CardSet cards ) {
		CardSet theClass = null;
		if ( cl == -1 ) {
			// create new class
			theClass = new CardSet();
			nbClasses++;
			classes.add( theClass );
			// Could be done better!
			leaves.add( board.getClass( cards.nextSetBit( 0 ) ) );
			samediff.add( initSameDiff( cards ) );
			wrong.add( new CardSet() );
		} else {
			theClass = classes.get( cl );
			updateSameDiff( samediff.get(cl), cards );
		}
		// Reassign same and diff
		theClass.addCards( cards );
	}

	public CardSet getCards( int cl ) {
		return classes.get( cl );
	}

	public void notify( ClassGame turn ) {
		if ( turn.getIssue() == false ) { // failed turn
			// if memory, I should get it...
			// I should get the intersection of their
			int cl = turn.getAssigned();
			if ( cl != -1 ) { // cannot memorise class creation
				int[] SD = samediff.get(cl);
				int diffBit = commonIncompatibilityBit( SD, turn.getCards() );
				CardSet forbidden = wrong.get( cl );
				if ( diffBit != -1 ) {
					// clevermem: if difference with pattern is 1, then discard all cards with this
					int value = SD[diffBit];
					for ( int i=0; i<NBCARDS; i++ ) {
						// Those cards which do not have the value of the pattern
						if ( Card.featureValue( i, diffBit ) != value ) {
							forbidden.set( i );
						}
					}
				} else if ( turn.getCards().cardinality() == 1 ) {
					// we know that this card is not in this class
					forbidden.addCard( turn.getCards().nextSetBit( 0 ) );
				}
			}
		} else { // successful turn
			// assign cards to class
			assignCards( turn.getAssigned(), turn.getCards() );
		}
	}

	/**
	 * If all incompatible bits (if any) are the same for all cards, return it
	 * else return -1
	 */
	private int commonIncompatibilityBit( int[] samediff, CardSet cards ) {
		int result = -1;
		for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
			int diff = onebitDifference( samediff, i );
			if ( diff == -2 ) return -1;
			if ( diff != -1 ) {
				if ( result == -1 ) result = diff;
				else if ( result != diff ) return -1;
			}
		}
		return result;
	}

	/**
	 * If a card is compatible with a pattern, return -1
	 * if it compatible but for one bit, return it,
	 * otherwise (more than one bit) return -2
	 */
	private int onebitDifference( int[] samediff, int card ) {
		int result = -1;
		for ( int i=NBFEATURES-1; i>=0; i-- ) {
			if ( samediff[i] != -1 && samediff[i] != Card.featureValue( card, i ) ) {
				if ( result == -1 ) result = i;
				else return -2;
			}
		}
		return result;
	}

	/**
	 * For each value of the card, 
	 * If record if it is compatible
	 * The resulting int is actually a bitset (encoding the conflicts)
	 */
	public int incompatibility( int card, int cl ) {
		// get the representation of cl (better above)
		int result = incompatibility( card, getSameDiff( cl ) );
		// ????
		//if ( result == 0 ) return -1;
		//else return result;
		return result;
	}

	// Characterises the incompatibility
	public int incompatibility( int card, int[] classDesc ) {
		int result = 0;
		// decode the instance (better above)
		// -1 = anything goes, other value.
		// risk when not -1 and different
		if ( classDesc[0] != -1 && classDesc[0] != Card.featureValue( card, 0 ) ) result += 8;
		if ( classDesc[1] != -1 && classDesc[1] != Card.featureValue( card, 1 ) ) result += 4;
		if ( classDesc[2] != -1 && classDesc[2] != Card.featureValue( card, 2 ) ) result += 2;
		if ( classDesc[3] != -1 && classDesc[3] != Card.featureValue( card, 3 ) ) result += 1;
		return result;
	}

	// Counts the incompatibilities
	public int incCount( int card, int[] classDesc ) {
		int result = 0;
		// decode the instance (better above)
		// -1 = anything goes, other value.
		// risk when not -1 and different
		if ( classDesc[0] != -1 && classDesc[0] != Card.featureValue( card, 0 ) ) result++;
		if ( classDesc[1] != -1 && classDesc[1] != Card.featureValue( card, 1 ) ) result++;
		if ( classDesc[2] != -1 && classDesc[2] != Card.featureValue( card, 2 ) ) result++;
		if ( classDesc[3] != -1 && classDesc[3] != Card.featureValue( card, 3 ) ) result++;
		return result;
	}

	protected boolean sameInc( int cl, int card1, int card2 ) {
		// get the representation of cl (better above)
		int[] samediff = getSameDiff( cl );
		if ( samediff[0] != -1 && samediff[0] != Card.featureValue( card2, 0 )
				&& samediff[0] == Card.featureValue( card1, 0 ) ) return false;
		if ( samediff[1] != -1 && samediff[1] != Card.featureValue( card2, 1 )
				&& samediff[1] == Card.featureValue( card1, 1 ) ) return false;
		if ( samediff[2] != -1 && samediff[2] != Card.featureValue( card2, 2 )
				&& samediff[2] == Card.featureValue( card1, 2 ) ) return false;
		if ( samediff[3] != -1 && samediff[3] != Card.featureValue( card2, 3 )
				&& samediff[3] == Card.featureValue( card1, 3 ) ) return false;
		return true;
	}

	// As a convenience for the users,
	// we maintain the set of features on which they have the same values and the set of features on which they have different values.
	/**
	 * Computes a structure assigning to eash feature
	 * -2 if non initialised
	 * -1 if indiferent
	 * the value common to all cards.
	 */
	protected int[] initSameDiff( CardSet cards ) {
		int SD[] = new int[NBFEATURES]; // -2 nonint; -1 any; 0.. values
		for ( int j=0; j<NBFEATURES; j++ ) SD[j] = -2;
		updateSameDiff( SD, cards );
		return SD;
	}

	protected void updateSameDiff( int[] SD, CardSet cards ) {
		// For all card in the bitset
		for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
			for ( int j=NBFEATURES-1; j>=0; j-- ) {
				// Decode the values
				setSDValue( SD, j, Card.featureValue( i, j ) );
			}
		}
	}

	protected boolean equalSameDiff( int[] SD1, int[] SD2 ) {
		for ( int j=NBFEATURES-1; j>=0; j-- ) {
			if ( SD1[j] != SD2[j] ) return false;
		}
		return true;
	}

	/*
	 * SD1 subsumes SD2
	 */
	protected boolean subsumeSameDiff( int[] SD1, int[] SD2 ) {
		for ( int j=NBFEATURES-1; j>=0; j-- ) {
			if ( ( ( SD1[j] != SD2[j] ) && ! ( SD1[j] == -1 ) ) ) return false;
		}
		return true;
	}

	private void setSDValue( int[] SD, int index, int value ) {
		if ( SD[index] == -2 ) { // not initialised
			SD[index] = value;
		} else if ( SD[index] > -1 && SD[index] != value ) SD[index] = -1;	
	}

	public int[] getSameDiff( int cl ) {
		return samediff.get( cl );
	}

	public CardSet getWrongs( int cl ) {
		return wrong.get( cl );
	}

	public Vector<int[]> getSameDiffs() {
		return samediff;
	}

	// Look in the classification if there is a corresponding leave
	// If yes... return it... otherwise, return null
	public CardSet isAClass( CardSet cards ) {
		// find the leave corresponding to an element
		int first = cards.nextSetBit( 0 );
		CardSet leave = board.getClass( first );
		// We first has to test that this class is not already here
		for ( CardSet knownClass: leaves ) {
			if ( knownClass == leave ) return null;
		}
		// The test for isInClass
		CardSet test = (CardSet)(cards.clone());
		test.andNot( leave );
		if ( test.isEmpty() ) {
			return leave;
		} else {
			return null;
		}
	}

	/**
	 * Where (and if) to save data to
	 */
	File refDir = null;
	File initDir = null;
	File finalDir = null;

	/**
	 * Where (and if) to load data from
	 */
	String loadDir = null;
	String loadOnto = null;
	boolean loadEnv = false;

	public ClassEnvironment() {
		NBFEATURES = Feature.NBFEATURES;
		NBCARDS = CardSet.NBCARDS;
	}

	public ClassEnvironment init() throws Exception {
		return init( new Properties() );
	}

	public ClassEnvironment init( Properties param ) throws Exception {
		deck = new Deck();
		classes = new Vector<CardSet>();
		leaves = new Vector<CardSet>();
		samediff = new Vector<int[]>();
		wrong = new Vector<CardSet>();

		if ( param.getProperty( "nbLevels" ) != null ) {
			NBLEVELS = Integer.parseInt( param.getProperty( "nbLevels" ) );
		}
		if ( param.getProperty( "nbLeaves" ) != null ) {
			NBLEAVES = Integer.parseInt( param.getProperty( "nbLeaves" ) );
		}

		// SAVE: new stuff
		if ( param.getProperty( "refDir" ) != null ) {
			refDir = new File( param.getProperty( "refDir" ) );
			refDir.mkdir();
			refDir = new File( refDir, "ClassEnvironment" );
		}
		if ( param.getProperty( "initDir" ) != null ) {
			initDir = new File( param.getProperty( "initDir" ), "ClassEnvironment" );
		}
		if ( param.getProperty( "finalDir" ) != null ) {
			finalDir = new File( param.getProperty( "finalDir" ), "ClassEnvironment" );
		}
		// LOAD
		if ( param.getProperty( "loadRunDir" ) != null ) {
			loadDir = param.getProperty( "loadRunDir" ) + "/init/";
			loadOnto = param.getProperty( "loadOnto" );
			if ( param.getProperty( "loadEnv" ) != null ) loadEnv = true;
		}

		// Could eventually get back!
		//if ( param.getProperty( "nbFeatures" ) != null && !param.getProperty( "nbFeatures" ).equals("") ) {
		//    NBFEATURES = Integer.parseInt( param.getProperty( "nbFeatures" ) );
		//}
		logger.debug( PlayClass.getString( "i5" ) );
		return this;
	}

	/*
	 * Imported from player class
	 */

	protected void printBoard( PrintStream os ) {
		for ( int cl = 0; cl < nbClasses; cl++ ) {
			os.print( "  "+PlayClass.getString( "cl" )+" "+cl+":" );
			CardSet classcards = getCards( cl );
			for ( int i = classcards.nextSetBit(0); i >= 0; i = classcards.nextSetBit(i+1)) {
				os.print( "\t"+Card.toPattern( i ) );
			}
			os.println();
		}
	}

	// nbClasses
	protected int[][] getIncompatibility ( CardSet hands ) {
		int[][] result = new int[nbClasses][NBCARDS]; // initialised with 0
		for ( int i = hands.nextSetBit(0); i >= 0; i = hands.nextSetBit(i+1)) {
			for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
				// Pattern instead of nuber
				result[cl][i] = incompatibility( i, cl );
			}
		}
		return result;
	}

	public String classification() {
		return board.toString();
	}

	

	/*
	 * I/O
	 */

	public void logInit() throws Exception {
		saveClass( initDir );
	}

	public void logFinal() throws Exception {
		saveClass( finalDir );
	}

	// SAVE
	public void saveClass( File dir ) {
	}

}


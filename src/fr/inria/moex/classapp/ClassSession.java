/*
 * Copyright (C) INRIA, 2021-2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Experiment for class game:
 * java -Dlog.level=DEBUG -cp lib/lazylav/ll.jar:lib/slf4j/logback-classic-1.2.3.jar:lib/slf4j/logback-core-1.2.3.jar:. fr.inria.exmo.lazylavender.engine.Monitor -Dexperiment=package fr.inria.moex.classgame.ClassSession -DclassStrategy=SAFE
 * 
 * Parameters:
 * nbAgents = (3)
 * nbLevels = 1-4 ()
 * nbLeaves = 1-81 ()
 * nbInitCards = (5)
 * interactive = 0-N (0)
 * see below for the strategies
 * classStrategy = RANDOM | INTERACTIVE | SAFE | RISK
 * classKnowerStrategy = RANDOM
 */


// Would be nicer that these guys would be threads!
public class ClassSession { 
    final static Logger logger = LoggerFactory.getLogger( ClassSession.class );

    protected ClassPlayer[] agents;

    /**
     * Number of agents to play (-DnbAgents)
     */
    protected int numberOfAgents = 3;
    
    /**
     * Number of agents to play (-DnbInitCards)
     */
    protected int NBINITCARDS = 8;
    
    /**
     * D (-Dinteractive)o I create an interactive player
     */
    protected int INTERACT = 0;
    
    /**
     * For printing out information
     */
    private PrintStream os = System.out;

    public void setOutput( PrintStream stream ) {
	os = stream;
    }
    
    /**
     * -D player = fullclassname
     * This class should be available in the environment and implement ClassPlayer
     **/
    private String playerClassName = "fr.inria.moex.classapp.ClassPlayerAgent";
    
    /**
     * Subdirectories for saving data
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;
    
    // Where (and if) to save games to
    File saveGames = null;

    // Where (and if) to load games from
    String loadGames = null;

    protected ClassEnvironment env = null;
    
    protected int player;
    protected ClassPlayer knower;
    protected ClassPlayer lastInteractivePlayer; // JE2022: for the gui

    public ClassEnvironment getEnvironment() {
		return env;
    }

    public ClassPlayer getLastInteractivePlayer() {
		return lastInteractivePlayer;
    }

    public int getNumberOfAgents() {
		return numberOfAgents;
    }

    public ClassPlayer[] getAgents() {
		return agents;
    }
    
    public ClassSession() {}

    public void init( Properties param ) throws Exception {
		if ( param.getProperty( "player" ) != null ) {
			playerClassName = param.getProperty( "player" );
		}
		if ( param.getProperty( "nbAgents" ) != null ) {
			numberOfAgents = Integer.parseInt( param.getProperty( "nbAgents" ) );
		} else { // Set the default for sub processes
			param.setProperty( "nbAgents", Integer.toString( numberOfAgents ) );
		}
		if ( param.getProperty( "nbInitCards" ) != null ) {
			NBINITCARDS = Integer.parseInt( param.getProperty( "nbInitCards" ) );
		}
		if ( param.getProperty( "interactive" ) != null ) {
			try {
				INTERACT = Integer.parseInt( param.getProperty( "interactive" ) );
			} catch ( NumberFormatException nfex ) {
				INTERACT = 1;
			}
		}
		if ( param.getProperty( "replayGames" ) != null ) {
			loadGames = param.getProperty( "loadRunDir" );
		}
		// JE2018: why not creating it before? where
		String runDir = param.getProperty( "runDir" );
		if ( runDir != null ) {
			File runDirD = new File( runDir );
			runDirD.mkdir();
			if ( param.getProperty( "saveInit" ) != null ) {
				initDir = new File( runDir, "init" );
				param.setProperty( "initDir", initDir.toString() );
			}
			if ( param.getProperty( "saveFinal" ) != null ) {
				finalDir = new File( runDir, "final" );
				param.setProperty( "finalDir", finalDir.toString() );
			}
			if ( param.getProperty( "saveRef" ) != null ) { // Is there another one?
				refDir = new File( runDir, "reference" );
				param.setProperty( "refDir", refDir.toString() );
			}
			if ( param.getProperty( "saveGames" ) != null ) {
				saveGames = new File( runDir, "games.tsv" );
			}
		}
		if ( INTERACT > numberOfAgents ) {
			throw new Exception ( PlayClass.getString( "e1a" )+INTERACT+PlayClass.getString( "e1b" )+numberOfAgents );
		}
		if ( param.getProperty( "loadEnv" ) != null || param.getProperty( "loadAgents" ) != null ) {
			param.setProperty( "loadOnto", "agent" ); // name of the directory containing ontologies
		}
		env = new ClassEnvironment();
		env.init( param );
		if ( numberOfAgents * NBINITCARDS >= ClassEnvironment.getNbCards() ) {
			throw new Exception( PlayClass.getString( "tma" )+numberOfAgents );
		}
		
		// Create agents
		agents = new ClassPlayer[numberOfAgents];
		// Draw randomly the knower
		Random agentFinder = new Random(); // could use setSeed( long seed );
		player = agentFinder.nextInt( numberOfAgents );
		
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			if ( ag == player ) {
				agents[ag] = new ClassPlayerAgent( env, player );
			} else {
				agents[ag] = createAgent( env, ag );
			}
			agents[ag].init( param );
		}
		knower = agents[ player ];
		// The knower does not know yet, it is knower
		// Save and log
		initSaveAgents();
		env.logInit();
    }
	
    /**
     * Player creation
     **/
    
    private Constructor<?> playerConstructor;

    private ClassPlayer createAgent( ClassEnvironment env, int id ) throws Exception {
		//return new ClassPlayerAgent( env, id );
		try {
			if ( playerConstructor == null ) {
				Class<?> playerClass = Class.forName( playerClassName );
				Class<?>[] cparams = { ClassEnvironment.class, int.class };
				playerConstructor = playerClass.getConstructor(cparams);
			}
			Object[] mparams = { env, id };
			return (ClassPlayer)playerConstructor.newInstance( mparams );
		} catch ( Exception ex ) {
			logger.error( "Cannot create players {}", playerClassName );
			throw ex;
		}
    }
	
    public ClassPlayer[] getAgentTable() {
		return agents;
    }
    
    /**
     * Classification
     **/
    
    private Classification classif = null;

    // JE2023: Added here instead of in the environment 
    public Classification getClassification() {
		return classif;
    }
    
    /**
     * 
     **/
    
    public void initSession() throws Exception {
		Random agentFinder = new Random(); // could use setSeed( long seed );
		// Save and log
		PrintWriter prGames = null;
		if ( saveGames != null ) {
			try {
				prGames = new PrintWriter( saveGames );
			} catch (FileNotFoundException fnfex) {
				logger.debug( "IGNORED: file not found", fnfex );
			} finally {
				if ( prGames != null ) prGames.close();
			}
		}
		// load games
		// Could also use [what is more efficient?]
		// BufferedReader buf = new BufferedReader(new FileReader("/home/little/Downloads/test"));
		// spec = buf.readLine();
		Iterator<String> gameIt = null;
		if ( loadGames != null ) {
			try {
				gameIt = Files.lines( Paths.get( loadGames, "games.tsv") ).iterator();
				logger.debug( "Found games.tsv in {}", loadGames );
			} catch (IOException ioex) {
				logger.debug( "IGNORED: exception", ioex );
			}
		}
		// Init
		// - create board (ask environment)
		// These should be the number of levels and leaves (it is also stored in env...)
		classif = env.genBoard();
		knower.setKnower( classif );
		logger.debug( PlayClass.getString( "i11" ), classif );
		// Choose randomly those interactive agents
		for ( int i = 0; i < INTERACT; i++ ) {
			int interactive = agentFinder.nextInt( numberOfAgents );
			while ( interactive == player || agents[interactive].isInteractive() ) interactive = agentFinder.nextInt( numberOfAgents );
			agents[interactive].setInteractive();
			lastInteractivePlayer = agents[interactive]; // JE2022: for the gui
		}
		// - distribute cards (ask environment)
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			agents[ag].receiveCards( env.drawCards( NBINITCARDS ) );
		}
		// Should draw the initial card...
		env.assignCards( -1, env.drawCards( 1 ) );
		logger.debug( PlayClass.getString( "i14" ), env.getCards(0) );
    }
    
    protected ClassPlayer currentPlayer;
    protected int iteration;

    public void process() throws Exception {
		// Loop
		iteration = 0;
		while( true ) {
			playCore();
			// if no card, then exit
			if ( currentPlayer.nbCards() == 0 ) break;
			player++;
			if ( player == numberOfAgents ) player = 0;
		}
		// End of the game
		printResult( currentPlayer );
		// - count cards
		int nbcards = 0;
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			// JE2022: cancelled for genericity
			//agents[ag].displayCards();
			logger.debug( PlayClass.getString( "i13b" ), agents[ag].getId(), (agents[ag]==knower)?"*":"", agents[ag].nbCards() );
			
			nbcards += agents[ag].nbCards();
		}
		logger.info( PlayClass.getString( "r3" ), (iteration/numberOfAgents)+1, (float)nbcards/(float)(numberOfAgents-1), env.nbClasses() );
		// - ask them to guess
		Classification guess = Classification.guessClassification( env.getSameDiffs(), false );
		if ( guess == null ) {
			logger.info( PlayClass.getString( "r6" ) );
		} else {
			logger.debug( PlayClass.getString( "r4" ), guess );
			switch ( classif.compare( guess ) ) {
			case 0: logger.info( PlayClass.getString( "r5a" ) ); break;
			case 1: logger.info( PlayClass.getString( "r5b" ) ); break;
			case -1: logger.info( PlayClass.getString( "r5c" ) ); break;
			}
		}
		// If necessary for to repeat with another Knower
		env.logFinal();
		finalSaveAgents();
    }
    
    public ClassGame playCore() throws Exception {
		iteration++;
		currentPlayer = agents[player];
		ClassGame turn = new ClassGame( currentPlayer );
		printPlayer( currentPlayer );
		// Play
		currentPlayer.playGame( turn );
		logger.debug( PlayClass.getString( "turninfo" ), currentPlayer.getId(), turn.getCards().toPatternList(), turn.getAssigned() );
		// Decide
		turnIssue( turn );
		return turn;
    }
	
    public void turnIssue( ClassGame turn ) throws Exception {
		// New class penalty
		if ( turn.newClass() ) currentPlayer.receiveCards( env.drawCards( 1 ) );
		printTurn( turn );
		// Check the knower anyway!
		if ( knower.judgeTurn( turn ) ) {
			turn.setIssue( true );
			printIssue( "a1" );
			env.notify( turn );
			currentPlayer.releaseCards( turn.getCards() );
		} else {
			turn.setIssue( false );
			printIssue( "a2" );
			env.notify( turn );
			currentPlayer.receiveCards( env.drawCards( 1 ) );
		}
		// Notify the agents
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			agents[ag].acceptNotice( turn );
		}
    }
	
    /**
     * Creates the directory necessary for saving sessions
     */
    public void initSaveAgents() throws Exception {
		if ( initDir != null ) {
			initDir.mkdir();
			saveAgents( initDir );
		}
		if ( finalDir != null ) finalDir.mkdir();
    }
    
    public void finalSaveAgents() throws Exception {
		if ( finalDir != null ) {
			//finalDir.mkdir();
			saveAgents( finalDir );
		}
    }
    
    public void saveAgents( File dir ) throws Exception {
		File agentsdir = new File( dir, "agent" );
		agentsdir.mkdir();
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			File agdir = new File( agentsdir, ""+ag );
			agdir.mkdir();
			agents[ag].save( agdir );
		}
    }
    
    public Properties getParameters() {
		Properties params = new Properties();
		params.setProperty( "nbAgents", String.valueOf( numberOfAgents ) );
		params.setProperty( "nbInitCards", String.valueOf( NBINITCARDS ) );
		/* JE2022: This is implementation specific
		   if ( knower != null ) {
		   params.setProperty( "classStrategy", knower.getStrategy() );
		   params.setProperty( "classKnowerStrategy", knower.getKnowerStrategy() );
		   }*/
		if ( env != null ) {
			params.setProperty( "nbLeaves", String.valueOf( env.numberOfLeaves() ) );
			params.setProperty( "nbLevels", String.valueOf( env.numberOfLevels() ) );
		}
		//	params.setProperty( "nbRuns",  );
		params.setProperty( "interactive", String.valueOf( INTERACT ) );
		//params.setProperty( "hints", );
		return params;
    }
	
    /*
     * Printing Games for playing interactively
     */

    public void printPlayer( ClassPlayer player ) {
		if ( os == null ) return;
		os.print( PlayClass.getString( "player" ) );
		os.print( player.getId() );
		if ( player == knower ) os.print( "* (" );
		else os.print( " (" );
		os.print( player.nbCards() );
		os.println( ")" );
		env.printBoard( os );
    }

    public void printTurn( ClassGame turn ) {
		if ( os == null ) return;
		os.print( " >> " );
		os.print( PlayClass.getString( "player" ) );
		os.print( turn.getPlayer().getId() );
		if ( turn.getPlayer() == knower ) os.print( "*" );
		os.print( PlayClass.getString( "played" ) );
		os.print( turn.getCards().toPatternList() );
		if ( !turn.newClass() ) {
			os.print( PlayClass.getString( "onclass" ) );
			os.print( turn.getAssigned() );
		} else os.print( PlayClass.getString( "onnclass" ) );
		os.println();
    }
	
    public void printIssue( String label ) {
		if (os == null ) return;
		os.print( " *** " );
		os.print( PlayClass.getString( label ) );
		os.println( " ***" );
    }
	
    public void printResult( ClassPlayer player ) {
		if ( os == null ) return;
		if ( player.isInteractive() )
			os.print( PlayClass.getString( "ywon" ) );
		else {
			os.print( PlayClass.getString( "player" ) );
			os.print( player.getId()+" " );
			os.print( PlayClass.getString( "won" ) );
		}
		os.print( " (" );
		os.print( PlayClass.getString( "knower" ) );
		os.print( ": " );
		os.print( knower.getId() );
		os.println( ")" );
    }
}

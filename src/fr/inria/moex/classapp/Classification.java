/*
 * Copyright (C) INRIA, 2020-2025
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

/**
 * This class embed the content of the previous GenBoard class used to generate boards
 *
$ java Classification 1 81  
Cannot reach 81 leaves with 1 levels
$ java Classification 1 82
The number of leaves has to be an odd integer between 1 and 81
$ java Classification 1 83
The number of leaves has to be an odd integer between 1 and 81
$ java Classification 4 81
(number (color (shape (filling ()  ()  () )  (filling ()  ()  () )  (filling ()  ()  () ) )  (shape (filling ()  ()  () )  (filling ()  ()  () )  (filling ()  ()  () ) )  (filling (shape ()  ()  () )  (shape ()  ()  () )  (shape ()  ()  () ) ) )  (color (shape (filling ()  ()  () )  (filling ()  ()  () )  (filling ()  ()  () ) )  (shape (filling ()  ()  () )  (filling ()  ()  () )  (filling ()  ()  () ) )  (shape (filling ()  ()  () )  (filling ()  ()  () )  (filling ()  ()  () ) ) )  (filling (color (shape ()  ()  () )  (shape ()  ()  () )  (shape ()  ()  () ) )  (shape (color ()  ()  () )  (color ()  ()  () )  (color ()  ()  () ) )  (color (shape ()  ()  () )  (shape ()  ()  () )  (shape ()  ()  () ) ) ) ) 
$ java Classification 2 7
 (filling (color ()  ()  () )  ()  (color ()  ()  () ) ) 
$ java Classification 2 9
 (filling (shape ()  ()  () )  (shape ()  ()  () )  (number ()  ()  () ) ) 
$ java Classification 1 3
 (color ()  ()  () )
$ java Classification 2 3
 (filling ()  ()  () ) 
$ java Classification 2 3 -e
Cannot reach exactly 2 levels with 3 leaves
 */

package fr.inria.moex.classapp;

import java.util.Random;

import java.lang.ProcessBuilder;
import java.lang.Process;
import java.lang.Iterable;

import java.util.BitSet; // Used for classification guess mask!
import java.util.Vector;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Deque;
import java.util.ArrayDeque;

import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
//import static java.nio.charset.StandardCharsets.UTF_8;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Structure of the Classication board
 */

public class Classification implements Iterable<Classification> {
	final static Logger logger = LoggerFactory.getLogger( Classification.class );

	static int NBFEATURES = Feature.NBFEATURES;
	static int NBVALUES = Card.NBVALUES;

	// This is actually CardSet.NBCARDS
	public static int MAXLEAVES = (int)Math.pow( NBVALUES, NBFEATURES );

	static boolean LATEX = false;
	static boolean PNG = false;

	// The feature on which this node is split
	public Feature feature = null;

	// JE: pattern should be set for all classes...
	public int[] pattern;

	// The bitset corresponding to the class (or cards belonging there?)
	// This is actually the ClassPattern of the class... is it?
	// May be a CardSet... or not... but should be...
	public CardSet cards = null;

	// The subtree
	public Classification[] subTree = null;

	static Random generator = new Random();

	// JE2024: Empty top class
	public Classification() {
		//		pattern = new int[] = {-1, -1, -1, -1};
	}

	// JE2024: Empty top class divided by features
	public Classification ( Feature f ) {
		feature = f;
		if ( f != null ) {
			subTree = new Classification[NBVALUES];
		}
	}

	//************ THIS WAS HIDDEN AND MUST BE DISCLOSED
	
	public Classification( String specif ) {
		read( specif );
		fillPatterns();
		// This puts the lists of cards in leaves: this is necessary for the game
		fillLeaves();
	}

	/**
	 * Call it from an empty Classification
	 * It will create it
	 * specifications S=()|(lbl S S S)
	 *
	 */
	public void read( String specif ) {
		try { 
			StringReader reader = new StringReader( specif );
			Deque<Classification> stack = new ArrayDeque<Classification>();
			stack.push( this );
			int c;
			while((c = reader.read()) != -1) {
				switch ( c ) {
				case '(':
					Classification current = stack.pop();
					while((c = reader.read()) != -1) { if ( c != ' ' && c != '\t' ) break; }
					// Just an empty classification
					if ( c == ')' ) break; // result = new Classification();
					// There is a fork
					String ff = ""+(char)c;
					while((c = reader.read()) != -1 && c != ' ' && c != '\t' && c != '('  && c != ')' ) ff += (char)c;
					// Analyse ff
					if ( ff.equals( "COLOR" ) ) current.feature = Feature.COLOR;
					else if ( ff.equals( "SHAPE" ) ) current.feature = Feature.SHAPE;
					else if ( ff.equals( "FILLING" ) ) current.feature = Feature.FILLING;
					else if ( ff.equals( "NUMBER" ) ) current.feature = Feature.NUMBER;
					else throw new Exception( "Unrecognised feature: "+ff );
					current.subTree = new Classification[NBVALUES];
					for ( int i=0; i < NBVALUES; i++ ) {
						current.subTree[i] = new Classification();
						stack.push( current.subTree[i] );
					}
					// Go ahead...
					break;
				case ' ': break;
				case '\t': break;
				case ')': ; break;
				}
			}
		} catch ( Exception ex ) { 
            System.out.println( ex ); 
		}
	}

	// JE2024: Taken can be replaced by pattern
	public Classification( int level, int forks, boolean[] taken, boolean exact ) {
		if ( forks != 0) {
			// draw spliting feature
			feature = Feature.getRandomFeature();
			while ( taken[feature.ordinal()] ) feature = Feature.getRandomFeature();
			taken[feature.ordinal()] = true;
			// draw one branch to reach the requested level
			int maxBranch = exact?generator.nextInt(NBVALUES):-1;
			// build subtrees
			subTree = new Classification[NBVALUES];
			int maxSubForks = maxNbForks( level );
			int rest = forks-1;
			for ( int i=0; i < NBVALUES; i++ ) {
				int nbforks = 0;
				if ( !exact || i>maxBranch ) {
					nbforks = drawForks( Math.max(0,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest) );
				} else if ( exact && i==maxBranch ) {
					nbforks = drawForks( Math.max(level-1,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest) );
				} else { // before maxBranch save level-1 forks from max
					nbforks = drawForks( Math.max(0,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest-level+1) );
				}
				subTree[i] = new Classification( level-1, nbforks, taken, (i==maxBranch)?true:false );
				rest -= nbforks;
			}
			taken[feature.ordinal()] = false;
		}
		fillPatterns();
		// This puts the lists of cards in leaves: this is necessary for the game
		fillLeaves();
	}

	public boolean isLeave() {
		return ( feature == null );
	}

	// Really public
	public static Classification genBoard( int levels, int leaves, boolean exact ) throws Exception {
		if ( generator == null ) generator = new Random(); // not each time
		// check that leaves are possible
		if ( levels < 1 || levels > NBFEATURES ) {
			throw new Exception( PlayClass.getString( "e2" )+NBFEATURES );
		} else if ( leaves < 1 || (leaves & 1) == 0 || leaves > MAXLEAVES ) {
			throw new Exception( PlayClass.getString( "e3" )+MAXLEAVES );
		} else if ( leaves > Math.pow( NBVALUES, levels ) ) {
			throw new Exception( PlayClass.getString( "e4a" )+leaves+PlayClass.getString( "e4b" )+levels+PlayClass.getString( "e4c" ) );
		} else if ( exact && ( leaves < levels*(NBVALUES-1)+1 ) ) {
			throw new Exception( PlayClass.getString( "e5a" )+levels+PlayClass.getString( "e5b" )+leaves+PlayClass.getString( "e5c" ) );
		}
		int nbforks = (leaves-1)/(NBVALUES-1);
		logger.debug( PlayClass.getString( "i10" ), levels, leaves );
		return new Classification( levels, nbforks, new boolean[NBFEATURES], exact );
	}

	public boolean eqClass( Classification cl ) {
		if ( this == cl ) return true;
		// I must find the meaning of the classification:
		// could use cards, but must be filled
		for( int i = 0; i < NBFEATURES; i++ ) {
			if ( this.pattern[i] != cl.pattern[i] ) return false;
		}
		return true;
	}

	public int[] classPattern( int color, int shape, int filling, int number ) {
		pattern = new int[4];
		pattern[Feature.COLOR.ordinal()] = color;
		pattern[Feature.SHAPE.ordinal()] = shape;
		pattern[Feature.FILLING.ordinal()] = filling;
		pattern[Feature.NUMBER.ordinal()] = number;
		return pattern;
	}
	
    public Iterator<Classification> iterator() {
        return new ClassIterator( this );
    }

	/** 
	 * Make leave traversing an iterator!!
	 * Unclear that it works...
	 * But: first set patterns
	 * Then fill leaves with patterns...
	 */
	/**
	 * Fill the leaves with the bitset containing all instances
	 * To call from the top with all values to -1.
	 * Assumes that patterns are sets
	 */
	private void fillLeaves() {
		for ( Classification cl: this ) {
			if ( cl.subTree == null ) {
				cl.fillLeafFromPattern();
			}
		}
	}

	private void fillLeafFromPattern() {
		cards = new CardSet(); 
		// Fill it
		int color = pattern[Feature.COLOR.ordinal()];
		int shape = pattern[Feature.SHAPE.ordinal()];
		int filling = pattern[Feature.FILLING.ordinal()];
		int number = pattern[Feature.NUMBER.ordinal()];
		int[] vcolor, vshape, vfill, vnumber;
		//int[] pcolor = (color==-1)?{0,1,2}:{color};
		if ( color == -1 ) { vcolor = new int[] {0,1,2}; } else { vcolor = new int[] {color}; }
		if ( shape == -1 ) { vshape = new int[] {0,1,2}; } else { vshape = new int[] {shape}; }
		if ( filling == -1 ) { vfill = new int[] {0,1,2}; } else { vfill = new int[] {filling}; }
		if ( number == -1 ) { vnumber = new int[] {0,1,2}; } else { vnumber = new int[] {number}; }
		
		for ( int i: vcolor )
			//val += i*powers[]; ...
			for ( int j: vshape )
				for ( int k: vfill )
					for ( int l: vnumber )
						//cards.set( l*27+k*9+j*3+i );
						cards.addCard( l*27+k*9+j*3+i );
	}

	private void fillPatterns() { fillPatterns( -1, -1, -1, -1 ); }
	private void fillPatterns( int color, int shape, int filling, int number ) {
		pattern = classPattern( color, shape, filling, number );
		if ( subTree != null ) {
			for ( int i=0; i < NBVALUES; i++ ) {
				switch ( feature ) {
				case COLOR: subTree[i].fillPatterns( i, shape, filling, number ); break;
				case SHAPE: subTree[i].fillPatterns( color, i, filling, number ); break;
				case FILLING: subTree[i].fillPatterns( color, shape, i, number ); break;
				case NUMBER: subTree[i].fillPatterns( color, shape, filling, i ); break;
				}
			}
		}
	}

	/* New version uses iterator
	   private void collectLeaves( Vector<Classification> accumulator ) {
		if ( subTree == null ) accumulator.add( this );
		else {
			for ( int i=0; i < NBVALUES; i++ ) subTree[i].collectLeaves( accumulator );
		}
		}*/

	private void collectLeaves( Vector<Classification> accumulator ) {
		for ( Classification cl : this ) {
			if ( cl.subTree == null ) accumulator.add( cl );
		}
	}

	/**
	 * Classifying a card in a classification
	 **/
	public CardSet getClass( int card ) {
		//return getClass( card/27, (card%27)/9, (card%9)/3, card%3 );
		return getClass( Card.featureValue( card, 0), Card.featureValue( card, 1), Card.featureValue( card, 2), Card.featureValue( card, 3) );
	}

	public CardSet getClass( int number, int filling, int shape, int color ) {
		return getClassification( number, filling, shape, color ).cards;
	}

	public Classification getClassification( int number, int filling, int shape, int color ) {
		if ( feature == null ) {
			return this;
		} else {
			switch ( feature ) {
			case COLOR: return subTree[color].getClassification( number, filling, shape, color );
			case SHAPE: return subTree[shape].getClassification( number, filling, shape, color );
			case FILLING: return subTree[filling].getClassification( number, filling, shape, color );
			case NUMBER: return subTree[number].getClassification( number, filling, shape, color );
			default: return null; // or  throws Exception? (should never happen)
			}
		}
	}

	/* JE2024: not used (and buggy)
	public int[] getClassPattern( int number, int filling, int shape, int color ) {
		int[] result = new int[NBFEATURES];
		for ( int i = 0; i < NBFEATURES; i++ ) result[i] = -1;
		Classification subTree = this;		
		while ( subTree != null ) {
			switch ( subTree.feature ) {
			case COLOR: result[subTree.feature] = color; subTree = subTree[color];
			case SHAPE: result[subTree.feature] = shape; subTree = subTree[shape];
			case FILLING: result[subTree.feature] = filling; subTree = subTree[filling];
			case NUMBER: result[subTree.feature] = number; subTree = subTree[number];
			}
		}
		return result;
	}
	*/

	// Maximum number of forks of a tree with level levels
	private int maxNbForks( int level ) {
		int result = 0;
		for ( int i=1; i < level; i++ ) {
			result += (int)Math.pow( NBVALUES, i-1 );
		}
		return result;
	}

    // JE 2023: would be better to cache it to not recompute at each call
    public int nbNodes() {
    	int result = 0;
		for ( Classification cl: this ) result++;
    	return result;
    }

	/**
	 * The distance between two classification is computed as the ratio of common classes on the number of classes.
	 * It is a distance that can be used on ontologies
	 * This is not very optimised
	 */
    public double distance( Classification classif2 ) {
    	int nbClasses = 0;
    	int nbCommon = 0;
		if ( this == classif2 ) return 0.;
		for ( Classification cl: this ) {
			nbClasses++;
			for ( Classification cl2: classif2 ) {
				if ( cl.eqClass( cl2 ) ) {
					nbCommon++;
					break;
				}
			}
		}
		for ( Classification cl2: classif2 ) nbClasses++;
    	return (double)(nbClasses-2*nbCommon)/(double)nbClasses;
    }
	
	public int nbForks() {
		return (nbNodes()-1)/3;
	}

	// Compute in function of nbNodes
	public int nbLeaves() {
		return (2*nbNodes()+1)/3;
	}

	// draw a random integer between min and max
	private int drawForks( int minforks, int maxforks ) {
		int result = minforks+generator.nextInt( maxforks-minforks+1 );
		return result;
	}

	// The level of guess of the node (default safe, otherwise: incomplete; split but do not know how)
	public boolean safe = true;
	public boolean complete = true;
	public boolean certain = true;

	public boolean allSafe() {
		if ( feature == null || !safe ) return safe;
		for ( int i=0; i < NBVALUES; i++ ) {
			if ( ! subTree[i].allSafe() ) return false;
		}
		return true;
	}
	public boolean allComplete() {
		if ( feature == null || !complete ) return complete;
		for ( int i=0; i < NBVALUES; i++ ) {
			if ( ! subTree[i].allComplete() ) return false;
		}
		return true;
	}
	public boolean allCertain() {
		if ( feature == null || !certain ) return certain;
		for ( int i=0; i < NBVALUES; i++ ) {
			if ( ! subTree[i].allCertain() ) return false;
		}
		return true;
	}

	// Used to store a description of the class in case of guess
	public int[] classDesc;

	/**
	 * Here is a program which starts with an empty classification and a set of leaf class patterns
	 * and guesses the underlying classification
	 * 
	 * The guessed classification is the current classification
	 * The returned value tells if it has been guessed or not
	 *
	 * Tentative algorithm:
	 */
	public boolean guessClassification( Vector<int[]> patterns, BitSet mask, boolean forceCompletion ) {
		boolean forceSplit = false;
		if ( mask.cardinality() == NBFEATURES ) { // NO SPLIT POSSIBLE (leaf class)
			// safe and complete
			//System.err.print("-1");
			return true;
		}
		if ( patterns.size() == 0 ) { // NO EXAMPLES... UNCERTAIN, DO NOT SPLIT
			// Actually this goes like uncertain...
			complete = false;
			certain = false;
			//System.err.print("-2");
			return false;
		}
		int nbfeats = 0;       // The number of features on which a split can occur
		int featureIndex = -1; // The index of the last such feature
		// The discriminant features cannot be "*"
		for ( int i=0; i<NBFEATURES; i++ ){
			if ( !mask.get( i ) ) {
				boolean nosplit = false;
				for( int[] pattern: patterns ) {
					if ( pattern[i] == -1 ) nosplit = true; // cannot split
				}
				if ( !nosplit ) {
					nbfeats++;
					featureIndex = i;
				}
			}
		}
		if ( nbfeats == 0 ) { // REACHED A LEAVE WITH CERTAINTY
			//System.err.print("-3");
			return true;
		} else if ( patterns.size() > 1 ) { // WE NOW IT IS SPLIT
			if ( nbfeats == 1 ) {
				// Split on it that's it
				//System.err.print("-4");
			} else { // SEVERAL POSSIBILITY
				//System.err.print("-5");
				if ( forceCompletion ) safe = false;
				else { complete = false; return false; }
			}
		} else if ( patterns.size() == 1 ) { // MAY BE SPLIT OR NOT (uncertain)
			certain = false;
			if ( nbfeats == 1 ) { // SPLIT ON IT OR NOT
				//System.err.print("-6");
				if ( forceSplit ) safe = false;
				else { complete = false; return false; }
			} else { // SEVERAL POSSIBILITY
				//System.err.print("-7");
				if ( forceSplit ) safe = false;
				else { complete = false; return false; }
			}
		}
		// In each of the case above... there exists a featureIndex to split on.
		// modify the mask
		mask.set( featureIndex, true );
		// Unfortunately, this is badly encoded in little indian!
		feature = Feature.values()[NBFEATURES-featureIndex-1];
		// Here there is a single good feature (and it is set in the class!)
		subTree = new Classification[NBVALUES];
		Vector<int[]> subvect = new Vector<int[]>();
		for ( int i=0; i < NBVALUES; i++ ) {
			// get all patterns which have the value for the feature
			subvect.clear();
			for ( int[] pattern: patterns ) {
				if ( pattern[featureIndex] == i ) subvect.add( pattern );
			}
			// unclear how to do it here... JE2021
			subTree[i] = new Classification();
			subTree[i].classDesc = Arrays.copyOf( classDesc, NBFEATURES );
			// or feature?
			subTree[i].classDesc[featureIndex] = i;
			boolean result = subTree[i].guessClassification( subvect, mask, forceCompletion );
			//if ( !result ) return false;
		}
		mask.set( featureIndex, false );
		return true;
	}

	public static Classification guessClassification( Vector<int[]> patterns, boolean forceCompletion ) {
		Classification result = new Classification();
		result.classDesc = new int[NBFEATURES];
		for ( int i = 0; i < NBFEATURES; i++ ) result.classDesc[i] = -1;
		boolean status = result.guessClassification( patterns, new BitSet(), forceCompletion );
		if ( status ) return result;
		else return null;
	}

	public static Classification guessCurrentClassificationCS( Vector<CardSet> cardsets, boolean forceCompletion ) {
		Classification result = new Classification();
		result.classDesc = new int[NBFEATURES];
		for ( int i = 0; i < NBFEATURES; i++ ) result.classDesc[i] = -1;
		Vector<int[]> patterns = new Vector<int[]>();
		for ( CardSet cs: cardsets ) patterns.add( cs.pattern() );
		boolean status = result.guessClassification( patterns, new BitSet(), forceCompletion );
		if ( status ) return result;
		else return null;
	}

	public static Classification guessCurrentClassification( Vector<int[]> patterns, boolean forceCompletion ) {
		Classification result = new Classification();
		result.classDesc = new int[NBFEATURES];
		for ( int i = 0; i < NBFEATURES; i++ ) result.classDesc[i] = -1;
		result.guessClassification( patterns, new BitSet(), forceCompletion );
		result.fillPatterns();
		return result;
	}

	// This assumes that the classification has 'cards' attribute empty as provided by guessClassification
	public Vector<int[]> assignCardClasses( CardSet cardSet ) {
		for ( Card card: cardSet ) {
			int i = card.getRepr();
			Classification leave = getClassification( Card.featureValue( i, 0), Card.featureValue( i, 1), Card.featureValue( i, 2), Card.featureValue( i, 3) ); // That's the class in the classification
			if ( leave.cards == null ) leave.cards = new CardSet();
			leave.cards.addCard( i );
		}
		Vector<Classification> accu = new Vector<Classification>();
		collectLeaves( accu );
		Vector<int[]> result = new Vector<int[]>();
		for( Classification leave : accu ) {
			if ( leave.cards != null ) {
				result.add( leave.cards.pattern() );
			}
		}
		return result;
	}
	
	/**
	 * 0: same classification
	 * 1: incomplete classification (missing forks
	 * -1: incorrect classification
	 */
	public int compare( Classification cl ) {
		if ( feature == null ) {
			if ( cl.feature == null ) return 0;
			else return -1;
		} else if ( cl.feature == null ) {
			return 1;
		} else if ( feature != cl.feature ) {
			return -1;
		}
		if ( subTree == null ) {
			if ( cl.subTree == null ) return 0;
			return -1; // ??
		} else {
			if ( cl.subTree == null ) return 1;
			int result = 0;
			for ( int i=0; i < NBVALUES; i++ ) {
				int comp = subTree[i].compare( cl.subTree[i] );
				if ( comp == -1 ) return -1;
				if ( comp == 1 ) result = 1;
			}
			return result;
		}
	}

	/**
	 * java Classification OPT level leaves
	 * generates a classification board of at most levels and exactly leaves.
	 * OPT:
	 * -e: reach exactly levels
	 * -l: generate LaTeX for the board
	 * -p: compile the LaTeX into PNG
	 */ 
	public static void main(String[] args) {
		int level = -1;
		int leaves = -1;
		boolean exact = false;

		// parse command line arguments
		if ( args.length < 2 ) {
			System.err.println( PlayClass.getString( "e6" ) );
			System.exit(1);
		}
		for ( int i=0; i < args.length; i++ ) {
			String arg = args[i].trim();
			if ( arg.equals( "-e") ) {
				exact = true;
			} else if ( arg.equals( "-l") ) {
				LATEX = true;
			} else if ( arg.equals( "-p") ) {
				LATEX = true;
				PNG = true;
			} else {
				if ( level == -1 ) {
					level = Integer.parseInt( arg );
				} else {
					leaves = Integer.parseInt( arg );
				}
			}
		}

		try {
			Classification result =  genBoard( level, leaves, exact );

			if ( LATEX ) {
				result.printLaTexTree( level, leaves );
			} else {
				result.printTree();
				System.out.println();
			}
		} catch (Exception ex) {
			System.err.println( ex.getMessage() );
			System.exit(1);
		}
	}

	public String treeName( int levels, int leaves ) {
		long code = toString().hashCode() - (long)java.lang.Integer.MIN_VALUE;
		return leaves+"-"+levels+"-"+code;
	}

	public String toString() {
		if ( feature == null ) {
			return "()";
		} else {
			String result = "("+feature;
			for ( Classification sub : subTree ) {
				result += " "+sub.toString();
			}
			return result+")";
		}
	}
	// Alternative?
	public String printTreeToString() {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String result = null;
		//
		final String UTF_8 = StandardCharsets.UTF_8.name();
		try ( PrintStream ps = new PrintStream( baos, true, UTF_8 ) ) {
			printTree( ps );
			//new String( baos.toByteArray(), java.nio.charset.StandardCharsets.UTF_8 );		
			result = baos.toString( UTF_8 );
		} catch ( UnsupportedEncodingException ex ) {
		}
		return result;
	}

	// So far everything is generic (features/values are just numbers)
	// Below the output functions are specific to the Class? game

	// Print the board as a lisp-like structure indicating only the characteristics
	public void printTree() {
		printTree( System.out );
	}
	public void printTree( PrintStream out ) {
		out.print( " (" );
		if ( feature != null ) {
			switch ( feature ) {
			case COLOR: out.print( PlayClass.getString( "col" ) ); break;
			case SHAPE: out.print( PlayClass.getString( "sha" ) ); break;
			case FILLING: out.print( PlayClass.getString( "fil" ) ); break;
			case NUMBER: out.print( PlayClass.getString( "num" ) ); break;
			default: break;
			}
			for ( Classification sub : subTree ) {
				sub.printTree( out );
			}
		}
		out.print( ") " );
	}

	// JE: all these could be computed once and for all (because here it is computed many times)
	// interspace (in points?)
	private int intersp = 4; // cardmininterval??
	private int leaveWidth = 10; // size of normal?

	// The sibling distance
	private int leftSpan() {
		if ( feature == null ) return 0; // never called on these
		return subTree[0].rightWidth()+intersp+subTree[1].leftWidth();
	}

	private int rightSpan() {
		if ( feature == null ) return 0; // never called on these
		return subTree[1].rightWidth()+intersp+subTree[2].leftWidth();
	}

	// The space between branches
	private int leftWidth () {
		if ( feature == null ) return leaveWidth/2;
		return leftSpan()+subTree[0].leftWidth();
	}

	private int rightWidth () {
		if ( feature == null ) return leaveWidth/2;
		return rightSpan()+subTree[2].rightWidth();
	}

	// Generate the LaTeX corresponding to the tree
	public void printLaTexTree( int level, int leaves ) {
		PrintStream stdout = System.out;
		PrintStream ps = null;
		String treeName = treeName( level, leaves );
		File texFile = null;
		try {
			if ( PNG ) {
				// Open temporary file
				texFile = new File( System.getProperty("java.io.tmpdir"), treeName+".tex" );
				// Set System.out to it
				//ps = new PrintStream(new BufferedOutputStream(new FileOutputStream("toto.tex")), true);
				ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(texFile)), true);
				System.setOut( ps );
			}

			// multi={tikzpicture}, create -0 files
			System.out.println( "\\documentclass[convert]{standalone}");
			System.out.println();
			System.out.println( "\\usepackage{url}");
			System.out.println();
			System.out.println( "\\usepackage[bundle=class]{classdeck}");
			System.out.println();
			System.out.println( "\\begin{document}");
			System.out.println();
			System.out.println( "\\begin{tikzpicture}[scale=1.1,edge from parent fork down,");
			System.out.println( "      level/.style={sibling distance=\\cardmininterval},");
			System.out.println( "      level distance=2cm,");
			System.out.println( "      every node/.append style={inner sep=0,outer sep=0}]");
			System.out.print( "\\");
			printTikZ( "0", "\\jokerclasspattern", "\\jokerclasscolor", "\\jokerclassshape" );
			System.out.println( ";");
			System.out.println();
			System.out.print( "% ");
			printTree();
			System.out.println();
			System.out.println( "  \\draw (current bounding box.north west) node[anchor=north west] {\\tiny\\url{https://moex.inria.fr/mediation/class/}};");
			System.out.println( "  \\draw (current bounding box.north east) node[anchor=north east] {\\tiny  $"+treeName.replaceFirst("-", "/")+"$};");
			System.out.println( "\\end{tikzpicture}");
			System.out.println( "\\end{document}");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally { // Close it
			System.setOut( stdout );
			if ( PNG ) { ps.close(); } // unclear
		}
		if ( PNG ) {
			// Compile it using Runtime
			try { 
				// Command to create an external process
				File outDir = texFile.getParentFile();
				ProcessBuilder pb = new ProcessBuilder().inheritIO();
				pb.command( "pdflatex", "-shell-escape", texFile.toString() ); // -output-directory="+outDir+"
				pb.directory( outDir );
				Process proc = pb.start();
				proc.waitFor();
				System.out.println( "open "+outDir+"/"+texFile.getName().replaceFirst("[.][^.]+$", "")+".png" );
			} catch (Exception e) { 
				e.printStackTrace(); 
			}
		}
	}

	// Recursive printing of a subtree
	public void printTikZ( String number, String pattern, String color, String shape ) {
		if ( feature == null ) {
			System.out.print( "node {\\classpict{finalclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
		} else {
			switch (feature) {
			case COLOR:
				System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
				System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, pattern, "\\redclasscolor", shape ); System.out.print( "}");
				System.out.print( "\nchild {" ); subTree[1].printTikZ( number, pattern, "\\blueclasscolor", shape ); System.out.print( "}");
				System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, pattern, "\\greenclasscolor", shape ); System.out.print( "}");
				break;
			case SHAPE:
				System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
				System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, pattern, color, "\\firstclassshape" ); System.out.print( "}");
				System.out.print( "\nchild {" ); subTree[1].printTikZ( number, pattern, color, "\\secondclassshape" ); System.out.print( "}");
				System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, pattern, color, "\\thirdclassshape" ); System.out.print( "}");
				break;
			case FILLING:
				System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
				System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, "\\emptyclasspattern", color, shape ); System.out.print( "}");
				System.out.print( "\nchild {" ); subTree[1].printTikZ( number, "\\fillingclasspattern", color, shape ); System.out.print( "}");
				System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, "\\fullclasspattern", color, shape ); System.out.print( "}");
				break;
			case NUMBER:
				System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
				System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( "1", pattern, color, shape ); System.out.print( "}");
				System.out.print( "\nchild {" ); subTree[1].printTikZ( "2", pattern, color, shape ); System.out.print( "}");
				System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( "3", pattern, color, shape ); System.out.print( "}");
				break;
			}
		}
	}

}

class ClassIterator implements Iterator<Classification> {

	// Classification piles
    private Classification classif;

	static int NBFEATURES = Feature.NBFEATURES;
	static int NBVALUES = Card.NBVALUES;
	
	// We need to record the pile of super classes and where we are
	private Classification[] prevClasses;
	private int[] prevRank;
	private int current;

	ClassIterator( Classification cl ){
		classif = cl;
		prevClasses = new Classification[NBFEATURES+1];
		prevRank = new int[NBFEATURES+1];
		int current = -1;
    }
    public boolean hasNext(){
		return ( classif != null );
    }
    public Classification next() {
		Classification result = classif;
		// compute the next one
		if ( classif.subTree != null ) { // or feature: PUSH
			current++;
			prevClasses[current] = classif;
			prevRank[current] = 0;
			classif = prevClasses[current].subTree[prevRank[current]];
		} else { // we are in a leave: POP
			while ( current != -1 && prevRank[current] == NBVALUES-1 ) current--;
			//System.err.println( "C: "+current+"prevRank[current] : "+prevRank[current] );
			if ( prevRank[current] != NBVALUES-1 && prevClasses[current] != null ) { // NEXT
				prevRank[current]++;
				classif = prevClasses[current].subTree[prevRank[current]];
			} else {
				classif = null;
			}
		}
		// return the other
		return result;
    }
}


/**
 * TODO
 * the idea is that it returns classifications which are leaves...
 * We are not far!
 **/
/**
 * TODO
 * We want also iterate on all classes...
 **/
/*
class LeaveIterator implements Iterator<Classification> {
	
    //private int currentIndex = 0;
    private Classification classif;
	private int[] levels;
	
    LeaveIterator( Classification cl ){
		classif = cl;
		levels = new int[NBFEATURES];
		for ( int lev = 0; lev < NBFEATURES ; lev++ ) levels[lev] = -1;
    }
    public boolean hasNext(){
		return (classif != null);
    }
    public Card next() {
		Classification result = classif;
		last = 0;
	    // compute current
		for ( int lev = 0; lev < NBFEATURES ; lev++ ) {
			if ( levels[lev] != -1 ) {
				result = result.subTree[levels[lev]];
				last = lev;
			} else {
				break;
			}
		}
		for ( int j = last; j>= 0; j-- ) {
			if ( levels[j] == NBVALUES ) {
				levels[j] = -1;
			} else {
				last = j;
				break;
			}
		}
		if ( last == -1 ) {
			classif = null;
		} else {
			levels[last] = levels[last]+1;
		}
		// return the other
		return result;
    }
	}*/

/*
 * Copyright (C) INRIA, 2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Properties;
import java.io.File;

public interface ClassPlayer {

    /**
     * Housekeeping: implemented in abstract
     **/
    public ClassPlayer init( Properties param );

    public void save( File dir ) throws Exception;

    public void load( File dir ) throws Exception;

    /**
     * Knower interface: implemented in abstract
     **/
    public void setKnower( Classification classif );

    public boolean isKnower();

    public boolean judgeTurn( ClassGame game ) throws Exception;
    
    /**
     * Interactive interface: implemented in abstract
     **/
    public void setInteractive();
    
    public boolean isInteractive();

    /**
     * Helper interface: implemented in abstract
     **/

    public int getId();

    public int nbCards();

    public CardSet getHand();
    
    /**
     * Playing interface
     **/

    public void acceptNotice( ClassGame game );

    public void receiveCards( CardSet crds );

    public void releaseCards( CardSet crds );
    
    public void playGame( ClassGame game ) throws Exception;
    
}

/*
 * Copyright (C) INRIA, 2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Properties;

public class MyClassPlayer extends AbstractClassPlayer implements ClassPlayer {

    public MyClassPlayer( ClassEnvironment e, int id ) {
	super( e, id );
	System.out.println( "Created player "+getId() );
    }

    private CardSet[] cache;
    
    public MyClassPlayer init( Properties params ) {
	super.init( params );
	//String strat = params.getProperty( "classStrategy" );
	cache = new CardSet[CardSet.NBCARDS]; // creates a far too large cache
	System.out.println( "Initialised player "+getId() );
	return this;
    }

    @Override
    public void playGame( ClassGame game ) throws Exception {
	System.out.print( "Player "+getId()+" plays" );
	// Try something
	int nbClasses = getCurrentClassNumber();
	CardSet crds = new CardSet();
	for ( int cl=0; cl < nbClasses; cl++ ) { // Iterate on classes
	    // It is possible to look into the class through
	    // getCurrentClassCards( cl )
	    // If cache not ready initialise it
	    if ( cache[cl] == null ) cache[cl] = new CardSet();
	    for ( Card crd: getHand() ) { // Iterate on cards in the hand
		if ( !cache[cl].includes( crd ) ) { // If not played in class
			crds.addCard( crd ); // 
			game.play( crds, cl ); // play this card to this class
			cache[cl].addCard( crd );// cache it
			System.out.println( " card "+printCard( crd )+" to class "+cl );
			return; // we only play one card
		}
	    }
	} // Apparently, all cards have been played in all classes
	for ( Card crd: getHand() ) {
	    crds.addCard( crd ); // add the first card 
	    game.play( crds, -1 ); // play it in a new class
	    // it will be a success, we do not need to cache it
	    System.out.println( " card "+printCard( crd )+" to new class " );
	    return;
	}
    }

    // The issue of each move is notified
    @Override
    public void acceptNotice( ClassGame game ) {
	if ( game.getPlayerId() == getId() ) {
	    if ( game.getIssue() ) {
		System.out.println( "This was correct" );
	    } else {
		System.out.println( "This was not correct" );
	    }
	}
    }

    @Override
    public void receiveCards( CardSet cards ) {
	super.receiveCards( cards ); // will put the received cards in the hand
	System.out.println( "Player "+getId()+" receiced cards: " );
	for ( Card crd : cards ) { // print each cards characteristics
	    System.out.println( "  "+printCard( crd ) );
	}
    }

    @Override
    public void releaseCards( CardSet cards ) {
	super.releaseCards( cards ); // Will put the played cards out of the hand
    }

    private String printCard( Card crd ) {
	return crd.getNumber()+"-"+crd.getColor()+"-"+crd.getFilling()+"-"+crd.getShape();
    }
    
}


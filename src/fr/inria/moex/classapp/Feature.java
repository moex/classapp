/*
 * Copyright (C) INRIA, 2021-2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Random;

/**
 * The features found in the game
 */

public enum Feature {
	COLOR, SHAPE, FILLING, NUMBER;
	
	public static int NBFEATURES = values().length;
	private static Random random;

	public static Feature getRandomFeature() {
		if ( random == null ) random = new Random();
		return values()[random.nextInt(NBFEATURES)];
	}

	/* Return the right order! */
	public static enum Number { I, II, III };
	public static enum Filling { FILLED, HATCHED, EMPTY };
	public static enum Shape { SQUARE, TRIANGLE, CIRCLE };
	public static enum Color { RED, BLUE, GREEN };

	public static Number[] NUMBERS_INDEX = Number.values().clone();
	public static Filling[] FILLINGS_INDEX = Filling.values().clone();
	public static Shape[] SHAPES_INDEX = Shape.values().clone();
	public static Color[] COLORS_INDEX = Color.values().clone();
}

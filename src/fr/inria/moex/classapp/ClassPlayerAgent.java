/*
 * Copyright (C) INRIA, 2021-2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassPlayerAgent extends AbstractClassPlayer implements ClassPlayer {
    final static Logger logger = LoggerFactory.getLogger( ClassPlayerAgent.class );

    private Random rand = null;

    /**
     * Strategy for regular player (-DclassStrategy)
     */
    public enum PlayerStrategy {
	INTERACTIVE,             // will ask interactively
	RANDOM,                  // throw a random card to a random class (or new class)
	NORISK,                  // if cards are safe bets for one class, play them
	BESTFITTINGCLASS,        // play the largest set of cards with minimum violations
	LITTLE,                  // plays one card with minimal violation
	LOWRISK,                 // play the lower risk cards for one class
	CLASSIF,                 // TODO: uses the classification guess to play
	CLASSHIST                // TODO: use classification + history
	                         // TODO: use information theory on players
	// Extra strategy may work on the difference between classes
    };

    PlayerStrategy strategy = PlayerStrategy.RANDOM;

    /** extended from abstract There is a @ **/
    public void setInteractive() {
	strategy = PlayerStrategy.INTERACTIVE;
    }
    
    public boolean isInteractive() {
	return (strategy == PlayerStrategy.INTERACTIVE);
    }
    
    /**
     * Strategy for knower (-DclassKnowerStrategy)
     */
    public enum KnowerStrategy {
	FIRST,                  // first card that fits in a group
	FIRSTFULL,              // first group that intersect
	LARGEST,                // largest set of cards in a class
	MININFO                 // Minimise information (interval?)
    };
    private KnowerStrategy knowerStrategy = KnowerStrategy.FIRST;

    /** Should be moderately public... and moderately useful **/
    public String getStrategy() {
	return strategy.toString();
    }

    public String getKnowerStrategy() {
	return knowerStrategy.toString();
    }

    // It is now fully disconnected from the ontology URI which is given otherwise
    public ClassPlayerAgent( ClassEnvironment e, int id ) { // To be generalised
	super( e, id );
	rand = new Random();
    }

    public ClassPlayerAgent init( Properties params ) {
		super.init( params );
		String strat = params.getProperty( "classStrategy" );
		if ( strat != null ) {
			if ( strat.equals( "NORISK" ) ) strategy = PlayerStrategy.NORISK;
			else if ( strat.equals( "BESTFIT" ) ) strategy = PlayerStrategy.BESTFITTINGCLASS;
			else if ( strat.equals( "LITTLE" ) ) strategy = PlayerStrategy.LITTLE;
			else if ( strat.equals( "LOWRISK" ) ) strategy = PlayerStrategy.LOWRISK;
			else if ( strat.equals( "RANDOM" ) ) strategy = PlayerStrategy.RANDOM;
		}
		String kstrat = params.getProperty( "classKnowerStrategy" );
		if ( kstrat != null ) {
			if ( kstrat.equals( "ONECARD" ) ) knowerStrategy = KnowerStrategy.FIRST;
			else if ( kstrat.equals( "FIRST" ) ) knowerStrategy = KnowerStrategy.FIRSTFULL;
			else if ( kstrat.equals( "LARGEST" ) ) knowerStrategy = KnowerStrategy.LARGEST;
			else if ( kstrat.equals( "MININFO" ) ) knowerStrategy = KnowerStrategy.MININFO;
		}
		
		logger.debug( PlayClass.getString( "i7" ), envid );
		return this;
    }
	
    @Override
	//@SuppressWarnings("fallthrough"): will be raised = OK
	public void playGame( ClassGame game ) throws Exception {
		nbClasses = env.nbClasses();

		if ( knower ) {
			CardSet cardsToPlay = new CardSet(); // Cards to play
			int classToPlay = -1; // Class where to play it
			switch (knowerStrategy) {
			case FIRST: // plays the first card that fits one class
				for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
					for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
						if ( env.getLeave( cl ).includes( i ) ) {
							cardsToPlay.set( i );
							game.play( cardsToPlay, cl );
							return;
						}
					}
				}
				// No card goes in a class, classToPlay is -1
				classToPlay = tryNewCard( cardsToPlay );
				break;
			case FIRSTFULL: // plays all the cards of the first class found
				// return a move (in a Game)
				for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
					for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
						if ( env.getLeave( cl ).includes( i ) ) {
							for ( i = cards.nextSetBit(i); i >= 0; i = cards.nextSetBit(i+1)) {
								if ( env.getLeave( cl ).includes( i ) ) {
									cardsToPlay.addCard( i );
								}
							}
							game.play( cardsToPlay, cl );
							return;
						}
					}
				}
				// No card goes in a class, classToPlay is -1
				classToPlay = tryNewCard( cardsToPlay );
				break;
			case MININFO: // plays the largest set of cards that give no information
				// Actually, if we can play a maximal set of cards that does no change a class pattern NOINFO (or that gives minimal info on a class pattern, MININFO, do it)
				// Compute classes all and diff
				CardSet matches = new CardSet();
				int largest = 0;
				// return a move (in a Game)
				for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
					int nbFound = 0;
					matches.clear();
					for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
						if ( env.incompatibility( i, cl ) == 0 ) { // FIT PATTERN
							matches.set( i );
							nbFound++;
						}
					}
					if ( nbFound > largest ) {
						largest = nbFound;
						classToPlay = cl;
						cardsToPlay.clear();
						cardsToPlay.or( matches );
					}
				}
				if ( classToPlay != -1 ) {
					game.play( cardsToPlay, classToPlay );
					return;
				};
				// Here, no else, no break: we go to largest
				// There is an option for the largest with minimum info of course...
			case LARGEST: // plays the largest set of cards fitting a class
				// Compute classes all and diff
				matches = new CardSet();
				largest = 0;
				// return a move (in a Game)
				for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
					int nbFound = 0;
					matches.clear();
					for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
						if ( env.getLeave( cl ).includes( i ) ) {
							matches.addCard( i );
							nbFound++;
						}
					}
					if ( nbFound > largest ) {
						largest = nbFound;
						classToPlay = cl;
						cardsToPlay.clear();
						cardsToPlay.or( matches );
					}
				}
				if ( classToPlay != -1 ) {
					game.play( cardsToPlay, classToPlay );
					return;
				} else { // No card goes in a class, classToPlay is -1
					classToPlay = tryNewCard( cardsToPlay );
				}
				break;
			}
			game.play( cardsToPlay, classToPlay );
		} else {
			// Look at the table
			// Certainly have some memory of what happens (memory of one self/memory of others)
			// Return the turn in memory
			// Look at the environment
			// Decide what to play
			chooseToPlay( game );
		}
    }

    /**
     * Choose a card at random
     */
    protected int tryNewCard( CardSet cardsToPlay ) {
		int card = rand.nextInt( NBCARDS );
		card = cards.nextSetBit( card );
		if ( card == -1 ) card = cards.nextSetBit( 0 );
		cardsToPlay.set( card );
		return -1;
    }

    public void chooseToPlay( ClassGame game ) {
		CardSet cardsToPlay = new CardSet();
		int cl = -1; // Class where to play it
		switch (strategy) {
		case INTERACTIVE:
			cl = getInteractivePlay( cardsToPlay );
			break;
		case NORISK: // if cards are safe bets for one class, play them
			// Compute classes all and diff
			cl = playNoRiskClass( getIncompatibility(), cardsToPlay );
			if ( cl == -2 ) cl = playRandom( cardsToPlay );
			else logger.debug( PlayClass.getString( "dp1" ), envid );
			break;
		case BESTFITTINGCLASS: // play the largest set of cards with minimal violation
			// Compute classes all and diff
			cl = playBestFittingClass( getIncompatibility(), cardsToPlay );
			if ( cl == -2 ) cl = playRandom( cardsToPlay );
			else logger.debug( PlayClass.getString( "dp2" ), envid );
			break;
		case LITTLE: // if no safe, play one card minimizing violation
			// Compute classes all and diff
			cl = playNoRiskClass( getIncompatibility(), cardsToPlay );
			if ( cl == -2 ) cl = playLittle( getIncompatibility(), cardsToPlay );
			else logger.debug( PlayClass.getString( "dp3" ), envid );
			break;
		case LOWRISK:   // play the lower risk cards for one class
			cl = playLowestRiskClass( getIncompatibility(), cardsToPlay );
			if ( cl == -2 ) cl = playRandom( cardsToPlay );
			else logger.debug( PlayClass.getString( "dp4" ), envid );
			break;
			// hole: create a new class... because split detected
			// memory?
			// Better that risk is 'espérance de gain'
			// 
		case CLASSIF:   // use an evaluation of the classification
			// compute the current classification
			Classification guess = Classification.guessCurrentClassification( env.getSameDiffs(), false );
			// assign each card to a class
			cl = playWithClassification( guess, getIncompatibility(), cardsToPlay );
			logger.debug( PlayClass.getString( "dp5" ), envid );
			break;
		case CLASSHIST:   // use an evaluation of the classification and the history!
			logger.debug( PlayClass.getString( "dp6" ), envid );
			break;
		case RANDOM: // Maximal add at most card
		default: // Minimal: add one card here
			cl = playRandom( cardsToPlay );
		}
		game.play( cardsToPlay, cl );	
    }

    private int getInteractivePlay( CardSet cardsToPlay ) {
		if ( hints ) printHints( os );
		else printHand( os );
		int cl = -2;
		while ( cl == -2 ) {
			try {
				System.out.println( PlayClass.getString( "q1" )+":" );
				Scanner in = new Scanner(System.in);
				String userChoice = in.nextLine();
				// Check that input is correct
				cl = Integer.parseInt( userChoice );
				if ( cl < -1 || cl >= nbClasses ) cl = -2;
				System.out.println( PlayClass.getString( "q2" )+cl+": " );
				userChoice = in.nextLine();
				for ( String cardString : userChoice.split("\\s+") ) {
					int card = Card.patternToInt( cardString );
					if ( card == -1 || !cards.get( card ) ) cl = -2;
					cardsToPlay.set( card );
				}
				if ( cl == -2 ) {
					System.out.print( "*** " );
					System.out.println( PlayClass.getString( "q3" ) );
					cardsToPlay.clear();
				}
			} catch ( Exception ex ) {
				System.out.print( "*** " );
				System.out.println( PlayClass.getString( "q3" ) );
				cardsToPlay.clear();
				cl = -2;
			}
		}
		return cl;
    }
    
    // A class (on the table) is a set of cards
    // It is possible to represent it as A pattern: for each feature: value if they all have it, * if there are different value (the pattern is initialised as the first class and then a or is ok)

    protected int playNoRiskClass( int[][] incompatibility, CardSet cardsToPlay ) {
		//printStatus();
		int bestClass = -2;
		int bestVal = 0;
		for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
			int val = 0;
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( nbInc( incompatibility[cl][i] ) == 0 ) { // this card is at risk 0
					val++;
				}
			}
			if ( val > bestVal ) { // this class has more 0 risk
				bestVal = val;
				bestClass = cl;
			}
		}
		if ( bestClass != -2 ) {
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( nbInc( incompatibility[bestClass][i] ) == 0 ) {
					cardsToPlay.set( i );
				}
			}
		}
		return bestClass;
    }
	
    /**
     * No best fitting class, try to play one card with minimal violation
     * And eventually, maximal information...
     * NOTRANDOMENOUGH
     */
    protected int playLittle( int[][] incompatibility, CardSet cardsToPlay ) {
		//printIncompatible( nbInc );
		int bestClass = -1;
		int bestVal = NBFEATURES+1;
		int bestCard = -1;
		for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
			// memory
			CardSet wrongs = env.getWrongs( cl );
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( !wrongs.get( i ) && nbInc( incompatibility[cl][i] ) < bestVal ) {
					bestVal = nbInc( incompatibility[cl][i] );
					bestClass = cl;
					bestCard = i;
					if ( bestVal == 1 ) break;
				}
				if ( bestVal == 1 ) break;
			}
		}
		// Here it would be more promising to identify this reasonable candidate
		// which maximises the size of the hands.
		if ( bestCard != -1 && bestVal < 3 ) { // we identified a reasonable candidate
			CardSet wrongs = env.getWrongs( bestClass );
			cardsToPlay.set( bestCard );
			// but check if no other candidates fit the bill!
			// i.e. same incompatibility?
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( !wrongs.get( i ) && env.sameInc( bestClass, bestCard, i ) ) {
					cardsToPlay.set( i );
				}
			}
		} else { // random otherwise
			// It is also possible to play random class... if quality not good enough!
			bestClass = tryNewCard( cardsToPlay );
		}
		return bestClass;
    }
	
    /**
     * - One may play the largest set of cards on the minimum violation
     * (this is a direct, but naive, generalisation of playBestFitting)
     * This works very badly in practice
     * moreover, the agent tends to always play the same... again and again
     */

    protected int playBestFittingClass( int[][] incompatibility, CardSet cardsToPlay ) {
		//printStatus();
		int lessRisky = NBFEATURES+1;
		int bestClass = -2;
		int bestVal = 0;
		for ( int cl = 0; cl < nbClasses; cl++ ) { // classes on the table
			int risk = lessRisky;
			int val = 0;
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( nbInc( incompatibility[cl][i] ) < risk ) { // risk lower
					risk = nbInc( incompatibility[cl][i] );
					val = 1;
				} else if ( nbInc( incompatibility[cl][i] ) == risk ) { // risk minimal
					val++;
				}
			}
			if ( risk < lessRisky ) { // new best risk
				lessRisky = risk;
				bestVal = val;
				bestClass = cl;
			} else if ( risk == lessRisky && val > bestVal ) { // new best class
				bestVal = val;
				bestClass = cl;
			}
		}
		if ( bestClass != -2 ) {
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( nbInc( incompatibility[bestClass][i] ) == lessRisky ) {
					cardsToPlay.set( i );
				}
			}
		}
		return bestClass;
    }
	
    // play those which belong to a closed class (safe)
    // play a new class to one that belongs to a non featured class (+open)
    // play those which belong to a possible class (?)
    // play a new class for those which belong to a subclass (%)

    // What is in the current classification:
    // safe: classes that are known
    //   = and not further subdivided
    // incomplete: do not know if further split
    //   = there is a class but nothing tell us that it is not split (not two classes+not all features in the class)
    // split: know that further split... but do not know how
    //   = we know that it is split because we have >= 2 subclasses, but they differ by more than 2 features
    // Then: some of these are on the table, some of these are not on the table
    // Then: each card belongs to one class here...
    // HENCE:
    // For each class we have safe/incomplete/split + HERE/not + #cards

    // Now:
	// safe,  table, N ---> risk=0 ; gain=N ; info=0 
	// safe,  not,   N ---> risk=0 ; gain=N-1 ; info=0 
	// inc,   table, N ---> risk=df ; gain=N ; info=1   (adding to an existing class no info on split)
	// inc,   not,   N ---> risk=1+df ; gain=N-1 ; info=1 (creating a split from existing no info on split)
	// split, table, N ---> risk=df ; gain=N ; info=1 (adding to an existing class not knowing how to split)
	// split, not,   N ---> risk=1+df ; gain=N-1 ; info=1 (creating a split not knowing the real split)
	// df=number of divergent features in the cards (minimized by playing 1 card)

	// SAFE = MAXgain( 1, 2 ), MAXgain( 3, 5 ), MAXgain( 4, 6 )
	//     or (better) MINrisk( 1, 2, 3, 5, 4, 6)
	// GAIN = MAXgain( 1, 2, 3, 5, 4, 6 )

	// Ultimate = add incompatibility (= histoy of the play) in the mix
	// (except that I am not sure that it records the wrong -1)

	// There is also the strategy of in which order playng cards.
	// It may be preferrable to pick up first the '-1' (penalty assign to new classes
	// especially when the new class is played with one single object (Nmax) and when it does not involve risk

    protected int playWithClassification( Classification guess, int[][] incompatibility, CardSet cardsToPlay ) {
		//printStatus();
		Vector<Leaf> leaves = new Vector<Leaf>();
		int idx = 0;
		// Iterate on all the leaves of the classification
		Stack<Classification> backtrack = new Stack<Classification>();
		backtrack.push( guess );
		while ( ! backtrack.empty() ) {
			Classification current = backtrack.pop();
			if ( current.feature != null ) {
				for ( Classification classif : current.subTree ) backtrack.push( classif );
			} else { // we are in a leave, treat it!
				Leaf lf = new Leaf( current );
				leaves.set( idx, lf );
				idx++;
				// This structure could be a Leave (local to here)
				// Is it on the table?
				int cl = 0;
				for ( ; cl < nbClasses; cl++ ) { // classes on the table
					// JE: THIS IS WRONG: THERE ARE ONLY SAMEDIFS FOR CLASSES ON THE TABLE...
					// OR IS IT A classDESC
					if ( env.equalSameDiff( current.classDesc, env.getSameDiff( cl ) ) ) {
						// Add this class to the leave
						lf.tableClass = cl;
						break;
						// TO BE IMPLEMENTED
					} else if ( env.subsumeSameDiff( current.classDesc, env.getSameDiff( cl ) ) ) {
						// We are in split
						// add this to classes
						// --- **** ---
					}
				}
				// What are the objects going there? In non, forget it...
				// Note that there is always one single leave for each object!
				CardSet cards = new CardSet();
				for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
					if ( env.incompatibility( i, current.classDesc ) == 0 ) {
						cards.set( i );
					}
				}
				lf.cards = cards;
			}
		}
		// HERE I REALLY HAVE A PROBLEM BECAUSE I DO NOT REMEMBER HOW
		// I PASS IT THE CLASS TO PLAY WHEN IT IS -1
		// At that point: leaves should contain all the leaves
		// Now evaluate these leaves with the cards we have...
		int bestClass = -2; // not sure that this should be bestclass
		int bestVal = -2;
		// Must depend on the strategies
		// Here, safe must be two different things:
		// object safe in safe classes...
		// Even if the class does not exists, it can be created
		// NOTE That here we do not look at the risk like before...
		// Introduce lf.risk()
		for ( Leaf lf : leaves ) {
			// SAFEST: allways safe first
			if ( lf.node.safe ) { // the class is safe to play
				if ( lf.value() > bestVal ) {
					bestClass = lf.tableClass;
					bestVal = lf.value();
				}
			} else if ( bestVal < 0 ) {
				if ( !(lf.node.complete) ) { // the class is incomplete
					// can be created with one card
					bestClass = lf.tableClass; // which is -1
					bestVal = 0;
				} else { // it is split but we do not know how
					// we will keep bestVal at -2
					// play one class in one element of the split at random.
					// It could also be good to play what is the most remote, but too difficult
				}
			}
		}
		return bestClass;
    }
	
    /**
     * Risk: the number of violations needed for this play 
     * Gain expectation: the number of classes that can be thrown
     *
     * - One may play the largest set of cards minimizing such violations
     * - 
     */

    protected int playRandom( CardSet cardsToPlay ) {
		// Random choose a card
		tryNewCard( cardsToPlay );
		logger.debug( PlayClass.getString( "q4" ), cardsToPlay.toPatternList() );
		// Random choose a class
		int cl = rand.nextInt( env.nbClasses()+1 )-1; // -1 = new class
		while ( true ) { 
			if ( cl == -1 ) {
				logger.debug( PlayClass.getString( "q5" ), cl );
				return cl;
			} else {
				// memory
				CardSet wrongs = env.getWrongs( cl );
				if ( !wrongs.intersects( cardsToPlay ) ) {
					return cl;
				} else {
					cl = rand.nextInt( env.nbClasses()+1 )-1; // -1 = new class
				}
			}
		}
    }
	
    /**
     * For each class find the set of cards which minimizes sum(penalties)/|set|
     * Let state it this way, retrieve the set of cards bringing minimum violation and maximal cardinality...
     */
    protected int playLowestRiskClass( int[][] incompatibility, CardSet cardsToPlay ) {
		//printStatus();
		int minViolations = NBFEATURES+1;
		int maxCardinality = 0;
		int bestClass = -2;
		CardSet bestHand = null;
		CardSet cannotBePlayed = (CardSet)cards.clone(); // those which cannot be allocated
		for ( int cl = 0; cl < nbClasses; cl++ ) {
			logger.trace( PlayClass.getString( "tr1" ), cl );
			// for each bit subsets
			// which card violates it
			int minClassViolations = NBFEATURES+2; // to break if no cards
			CardSet minViolationCards = null;
			// memory
			CardSet wrongs = env.getWrongs( cl );
			for ( int i = cards.nextSetBit(0); i >= 0; i = cards.nextSetBit(i+1)) {
				if ( !wrongs.get( i ) ) {
					cannotBePlayed.clear( i ); // it can be played
					int nbinc = nbInc( incompatibility[cl][i] );
					if ( nbinc < minClassViolations ) {
						minClassViolations = nbinc;
						if ( minViolationCards == null ) minViolationCards = new CardSet();
						else minViolationCards.clear();
						minViolationCards.set( i );
					} else if ( nbinc == minClassViolations ) {
						minViolationCards.set( i );
					}
				}
			}
			// minViolationCards contains all cards of minimum violations
			// now we compare those with minviolation
			logger.trace( PlayClass.getString( "tr2" ), minClassViolations );
			if ( minClassViolations <= minViolations ) {
				// Find the violation set of size minClassViolation
				// with the maximal cardinality
				int maxClassCardinality = 0;
				CardSet bestLocalHand = null;
				CardSet handToPlay = new CardSet();
				// for each card of minViolationCards
				// collect the cards with same violations (and discard them from minViolationCards)
				for ( int i = minViolationCards.nextSetBit(0); i >= 0; i = minViolationCards.nextSetBit(i+1)) {
					int cardinality = 1;
					handToPlay.clear();
					handToPlay.set( i );
					for ( int j = minViolationCards.nextSetBit(i+1); j >= 0; j = minViolationCards.nextSetBit(j+1)) {
						// If they have the same violations, literally
						if ( incompatibility[cl][i] == incompatibility[cl][j] ) {
							cardinality++;
							handToPlay.set( j );
							minViolationCards.clear( j );
						}
					}
					if ( cardinality > maxClassCardinality ) {
						maxClassCardinality = cardinality;
						bestLocalHand = handToPlay;
						handToPlay = new CardSet();
					}
				}
				logger.trace( PlayClass.getString( "tr3" ), maxClassCardinality );
				// Not better
				if ( minClassViolations < minViolations || maxClassCardinality > maxCardinality ) {
					minViolations = minClassViolations;
					maxCardinality = maxClassCardinality;
					bestClass = cl;
					bestHand = bestLocalHand;
				}
			}
		}
		logger.trace( PlayClass.getString( "tr4" ), bestClass );
		// if there are cards that belong to no class
		if ( minViolations > 0 ) {
			int toNewClass = cannotBePlayed.nextSetBit(0);
			if ( toNewClass >= 0 ) { // one cannot be assigned... play it to a new class
				cardsToPlay.set( toNewClass );
				return -1;
			}
		}	
		if ( bestClass == -2 ) return -2;
		// set cardsToPlay
		logger.trace( PlayClass.getString( "tr5" ), bestHand.toPatternList() );
		for ( int i = bestHand.nextSetBit(0); i >= 0; i = bestHand.nextSetBit(i+1)) {
			cardsToPlay.set( i );
		}
		return bestClass;
    }
	
    @Override
    public void acceptNotice( ClassGame game ) {}

    /*
    @Override
    public void receiveCards( CardSet crds ) {}
    
    @Override
    public void releaseCards( CardSet crds ) {}
    */
    
    // Use our compact notation?
    public void guessClassification( ) {
		
    }
}

class Leaf {
    public Classification node;
    public int tableClass = -1; // The eventual class on the table
    public CardSet cards; // The cards which actually fit in there
	
    public Leaf( Classification nd ) {
		node = nd;
		// initialize structure
    }
	
    public int value() {
		int result = cards.cardinality();
		if ( tableClass != -1 ) result = -1;
		return result;
    }
	
}

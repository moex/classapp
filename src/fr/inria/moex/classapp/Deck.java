/*
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classapp;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class defines the deck of cards
 * It extends CardSet, but the bit is set to true when the card is _not_ in the set
 */

public class Deck extends CardSet {
    final static Logger logger = LoggerFactory.getLogger( Deck.class );

    private Random rand = null;

	private static final long serialVersionUID = 7575757575757575757L;	

    public Deck() {
		rand = new Random(); // could use setSeed( long seed );
		logger.debug( PlayClass.getString( "i12" ) );
    }
	
    // These should be cancelled: there is no reason to look into the deck
    public boolean includes( int c ) {
		return !get( c ); // indeck if false
    }

    /**
     * Cards draws from the deck
     */
    public int drawCard() throws Exception { // draw values between 0 and 80
		// Check that it is not empty
		int card = nextClearBit( rand.nextInt( NBCARDS ) );
		if ( card > NBCARDS-1 ) card = nextClearBit( 0 );
		if ( card > NBCARDS-1 ) throw new Exception( PlayClass.getString( "i16" ) );
		flip( card ); // out of deck
		return card;
    }

    public CardSet drawCards( int nb ) throws Exception {
		CardSet cards = new CardSet();
		for ( int i = nb; i > 0; i-- ) {
			cards.addCard( drawCard() );
		}
		return cards;
    }

}

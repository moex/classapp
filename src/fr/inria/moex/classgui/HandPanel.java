/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Vector;
import java.util.Set;
import java.util.HashSet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.Card;
import fr.inria.moex.classapp.CardSet;
import fr.inria.moex.classapp.ClassPlayer;
import fr.inria.moex.classapp.ClassGame;

public class HandPanel extends JPanel {
    final static Logger logger = LoggerFactory.getLogger( HandPanel.class );

    private int nbRows = 1;
    
    private ClassGui gui;
    private CardHand cardsPanel;

    private Vector<ClassCard> cards = new Vector<ClassCard>();

    // JE: to put the represented player ... and then getting its hand!
    private ClassPlayer player;

	private static final long serialVersionUID = 7575757575757575757L;	

    public HandPanel( ClassGui gui ) {
		super();
		this.gui = gui;
		
		// Constraints with container
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weighty = 2;
		gbc.fill = GridBagConstraints.BOTH;
		
		cardsPanel = new CardHand( this );
		
		JScrollPane scrollHand = new JScrollPane( cardsPanel );
		scrollHand.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollHand.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollHand.setBorder( BorderFactory.createEmptyBorder() );
		gui.getFrame().add( scrollHand, gbc );
    }

    public void empty() {
		if ( cards != null ) {
			cards.clear();
			cardsPanel.removeAll();
		}
    }

    /**
     * La layout est mis à jour au moment où on change de
     * joueur actif (si il est INTERACTIVE) ou alors quand
     * le joueur actif vient de jouer.
     *
     * JE2023: That should be a method of CardHand if it were aware of cards...
     * JE2023: setCardLayout() will have to be called when resizing windows as it 
     * will recompute the numer of cards per rows. I did not manage
     */
    private void setCardsLayout() {
		cardsPanel.removeAll();

		int maxNbCardPerRow = (int)cardsPanel.getSize().getWidth() / (ClassCard.INIT_CARD_SIZE + 10 );
		nbRows = cards.size()/maxNbCardPerRow + ((cards.size()%maxNbCardPerRow==0) ? 0 : 1);

		// JE2023: This stuff is 
		GridBagConstraints card_gbc;
	
		// Puis on les remet avec leur nouveau gbc
		int nbCards = cards.size();
		int i=0;
		for ( ClassCard card : cards ) {
			card_gbc = new GridBagConstraints();
			card_gbc.gridx = i%maxNbCardPerRow;
			card_gbc.gridy = i/maxNbCardPerRow;
			card_gbc.weightx = getWidth()/maxNbCardPerRow;
			card_gbc.weighty = getHeight()/nbRows;
			card_gbc.insets = CardHand.DEFAULT_CARD_INSETS;
			cardsPanel.add( card, card_gbc );
			i++;
		}
    }

    public void updateCards( ClassGame lastTurn ) {
		CardSet toadd = new CardSet();
		CardSet current = new CardSet();
		for ( ClassCard c : cards ) current.addCard( c.getNum() );
		
		for ( Card c : getHand() ) {
			if ( !current.includes( c.getRepr() ) ) toadd.addCard( c );
		}
		
		// add a new card (where from? the ground???
		if ( lastTurn.getIssue() ) { // success
			Set<ClassCard> toremove = new HashSet<ClassCard>();
			// fetch cards to suppress
			for ( ClassCard c : cards ) {
				if ( c.isSelected() ) toremove.add(c);
			}
			cards.removeAll( toremove ); // Does not seem to work...
		} else { // failure
			// unselect all cards (or the ones)
			for ( ClassCard c : cards ) {
				if ( c.isSelected() ) {
					c.toggleSelection(); // looks bad, right!
					toggleCardSelection( c );
				}
			}  // Does not seem to work either
		}
		for ( Card c : toadd ) {
			cards.add( new ClassCard( gui, c.getRepr() ) );
		}
		
		// redraw
		setCardsLayout();
    }

    public CardSet getHand() {
		// JE: funnyly, we do not know who we are!
		return gui.getSession().getLastInteractivePlayer().getHand();	
    }
    
    /**
     * Selected cards are only those selected in the GUI.
     * Various modeles access here to see what is selected.
     * Which is returned as a cardset.
     */
    
    public void toggleCardSelection( ClassCard card ) {
		cardsPanel.toggleCardSelection( card );
    }
    
    public CardSet getSelectedCards() {
		CardSet selectedCards = new CardSet();
		for ( ClassCard c : cards ) {
			if ( c.isSelected() ) selectedCards.set( c.getNum() );
		}
		return selectedCards;
    }

    @Override
    public Dimension getPreferredSize() {
		Dimension dim = getParent().getSize();
		return new Dimension((int)(dim.getWidth()*0.9), (int)(dim.getHeight()*0.2));
    }
}

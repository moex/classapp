/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
    
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.CardSet;

public class TurnPanel extends JPanel {
    final static Logger logger = LoggerFactory.getLogger( TurnPanel.class );

    public static final Color BG_COLOR = new Color(50, 200, 50);

    private ClassGui gui;

    private JLabel playerPlaysOnLabel = null;
    private JPanel cardsPanel = null;
    private JLabel issueLabel = null;

	private static final long serialVersionUID = 7575757575757575757L;	

    public TurnPanel( ClassGui gui ) {
		super();
		this.gui = gui;
		setLayout( new GridBagLayout() );
		setBackground( BG_COLOR );

		// ---------------- PLAYER AND CLASS PANEL ---------------- //
		JPanel playerPlaysOnPanel = new JPanel();
		playerPlaysOnPanel.setBackground(BG_COLOR);
		playerPlaysOnLabel = new JLabel();
		playerPlaysOnLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		GridBagConstraints playerPlaysOn_gbc = new GridBagConstraints();
		playerPlaysOn_gbc.gridx = 0;
		playerPlaysOn_gbc.gridy = 0;
		playerPlaysOn_gbc.weightx = 15;
		playerPlaysOn_gbc.fill = GridBagConstraints.BOTH;
		playerPlaysOn_gbc.insets = new Insets(0, 0, 0, 0);
		playerPlaysOnPanel.add(playerPlaysOnLabel);
		
		add(playerPlaysOnPanel, playerPlaysOn_gbc);
		
		// ---------------- CARDS PANEL ---------------- //
		cardsPanel = new JPanel();
		GridBagConstraints cards_gbc = new GridBagConstraints();
		cards_gbc.gridx = 1;
		cards_gbc.gridy = 0;
		cards_gbc.weightx = 70;
		cards_gbc.fill = GridBagConstraints.BOTH;
		cards_gbc.insets = new Insets(0, 0, 0, 0);
		//		cardsPanel.setBackground(BG_COLOR);
		cardsPanel.setBackground(new Color(100, 200, 200));
		cardsPanel.setLayout(new GridBagLayout());
		
		add(cardsPanel, cards_gbc);
		
		// ---------------- ISSUE PANEL ---------------- //
		JPanel issuePanel = new JPanel();
		issuePanel.setBackground(BG_COLOR);
        issueLabel = new JLabel();
		issueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints issue_gbc = new GridBagConstraints();
		issue_gbc.gridx = 2;
		issue_gbc.gridy = 0;
		issue_gbc.weightx = 15;
		issue_gbc.fill = GridBagConstraints.BOTH;
		issue_gbc.insets = new Insets(0, 0, 0, 0);
		issuePanel.add(issueLabel);
		
		add( issuePanel, issue_gbc );
		
    }
    
    public void displayTurn( ClassGame turn ) {
		// Display left text
		if ( turn.getAssigned() == -1 ) {
			playerPlaysOnLabel.setText("<html>"+PlayClass.getString( "player" )+turn.getPlayer().getId() + "<br />" +
									   PlayClass.getString( "played" )+" <br /> "+PlayClass.getString( "onnclass" )+"</html>");
		} else {
			playerPlaysOnLabel.setText("<html>"+PlayClass.getString( "player" )+turn.getPlayer().getId() + "<br />" +
									   PlayClass.getString( "played" )+" <br /> "+PlayClass.getString( "onclass" )+ turn.getAssigned() +"</html>");
		}
		// Display cards
		// Remove everything from card panel
		cardsPanel.removeAll();
		// On place les cartes jouées dans ce turn
		CardSet cards = turn.getCards();
		for ( int c = cards.nextSetBit(0); c >= 0; c = cards.nextSetBit(c+1)) {
			ClassCard card = new ClassCard( gui, c );
			GridBagConstraints card_gbc = new GridBagConstraints();
			card_gbc.gridx = c;
			card_gbc.fill = GridBagConstraints.NONE;
			cardsPanel.add(card, card_gbc);
		}
		// Display right text
		issueLabel.setText("<html>"+PlayClass.getString( "issue" )+" :" + "<br>" +
						   (turn.getIssue()?"** "+PlayClass.getString( "a1" )+" **":"** "+PlayClass.getString( "a2" )+" **")+"</html>");
    }

    public void empty() {
		playerPlaysOnLabel.setText("");
		cardsPanel.removeAll();
		issueLabel.setText("");
    }
}

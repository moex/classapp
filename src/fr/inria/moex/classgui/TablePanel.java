/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Vector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
    
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.CardSet;

public class TablePanel extends JPanel implements MouseListener {
    final static Logger logger = LoggerFactory.getLogger( TablePanel.class );

    public static final Color BG_COLOR = new Color(50, 200, 50);

    private ClassGui gui;

    private PilesPanel pilesPanel;
    private TurnPanel turnPanel;
	
    private Vector<ClassPile> piles;

	private static final long serialVersionUID = 7575757575757575757L;	

    public TablePanel( ClassGui gui ) {
		super();
		this.gui = gui;

		// Layout
		setLayout(new GridBagLayout());
		setBackground(BG_COLOR);
		addMouseListener(this);
		
		// Constraints within container
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 7;
		gbc.weighty = 8;
		gbc.fill = GridBagConstraints.BOTH;
		// Inner components //
		gui.getFrame().add( this, gbc );
		
		// Piles panel
		pilesPanel = new PilesPanel( gui );
		initScrollBar( pilesPanel );
		
		piles = new Vector<ClassPile>();	
		// No hand yet
    }

    public void empty() {
		piles.clear();
		pilesPanel.empty();
		turnPanel.empty();
    }

    public void initScrollBar( PilesPanel pp ) {
		JScrollPane scrollPane = new JScrollPane( pp );
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBorder( BorderFactory.createEmptyBorder() );
		
		GridBagConstraints pilesPanel_gbc = new GridBagConstraints();
		pilesPanel_gbc.gridx = 0;
		pilesPanel_gbc.gridy = 0;
		pilesPanel_gbc.weightx = 100;
		pilesPanel_gbc.weighty = 100;
		pilesPanel_gbc.fill = GridBagConstraints.BOTH;
		
		add( scrollPane, pilesPanel_gbc );
    }

    public void showTurn( ClassGame turn ) {
		if ( turn == null ) return;
		if ( turnPanel == null ) {
			turnPanel = new TurnPanel( gui );
			turnPanel.setBackground(BG_COLOR);
			GridBagConstraints turnPanel_gbc = new GridBagConstraints();
			turnPanel_gbc.gridx = 0;
			turnPanel_gbc.gridy = 1;
			turnPanel_gbc.fill = GridBagConstraints.BOTH;
			turnPanel_gbc.insets = new Insets(0, 0, 0, 0); // TODO Supprimer
			add( turnPanel, turnPanel_gbc );
		}
		turnPanel.displayTurn( turn );
    }
    
    public void updateTable( ClassGame currentTurn ) {
		if ( currentTurn.getIssue() ) addTurnToClass( currentTurn );
		showTurn( currentTurn ); // JE2023: what about putting this in the if?
    }

    public void addTurnToClass( ClassGame turn  ) {
		int playedClass = turn.getAssigned();
		if ( playedClass == -1 ) {
			playedClass = addClassToTable();
		}
		addCardsToClass( playedClass, turn.getCards() );
    }
	
    public void addCardsToClass( int cl, CardSet cards ) {
		piles.get( cl ).addCards( cards );
    }
	
    public int addClassToTable() {
		int classNumber = piles.size();
		ClassPile pile = new ClassPile( gui, classNumber );
		piles.add( pile );
		GridBagConstraints pile_gbc = new GridBagConstraints();
		pile_gbc.gridx = 0;
		pile_gbc.gridy = classNumber; // JE2022: not sure it should be but why not!
		pile_gbc.fill = GridBagConstraints.BOTH;
		pile_gbc.insets = new Insets(5, 0, 5, 0);
		pilesPanel.add( pile, pile_gbc );
		return classNumber;
    }


    @Override
    public void mouseClicked( MouseEvent e ) {
		// TODO Auto-generated method stub
    }

    @Override
    public void mousePressed( MouseEvent e ) {
		/* Si on clique n'importe où sur la table sauf
	     * sur une carte, on crée une nouvelle classe. */
		CardSet selectedCards = gui.getHandPanel().getSelectedCards();
		try {
			/* On ne le fait que si au moins une carte est sélectionnée
			 * et que la session n'est pas entrain de faire jouer les bots
			 */
			if ( selectedCards.cardinality()>0 && !gui.getSession().isABotPlaying() )
				gui.getSession().play( -1, selectedCards );
		} catch ( Exception ex ) {
			ex.printStackTrace();
		}
    }

	@Override
	public void mouseReleased( MouseEvent e ) {
	    // TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	public ClassGui getGui() {
	    return gui;
	}
    
	/*@Override
	public Dimension getPreferredSize() {
		Dimension dim = getParent().getSize();
		return new Dimension((int)(dim.getWidth()*0.7), (int)(dim.getHeight()*0.3));
	}*/
}

/*
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import javax.swing.JComboBox;

import fr.inria.moex.classapp.Feature;

public class ExtendibleClass {
	String [] feature = new String [] {"Color", "Filling", "Shape" , "Number", "Nothing"};
	boolean [] chosenFeat = new boolean [] {false, false, false, false, false};
	Feature feat;

	private static final long serialVersionUID = 7575757575757575757L;	
	
	private void updateList () {
	}
	
	JComboBox featBox = new JComboBox();
	
	protected void chosenFeat ( ) {
		
	}
}

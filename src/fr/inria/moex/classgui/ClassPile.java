/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Vector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassEnvironment;
import fr.inria.moex.classapp.CardSet;

/**
 * ClassPile displays a group of cards on the table
 */
public class ClassPile extends JPanel implements MouseListener {
    final static Logger logger = LoggerFactory.getLogger( ClassPile.class );

    public static final Color BG_COLOR = new Color(200, 200, 200);

    private Vector<ClassCard> cards = new Vector<ClassCard>();
    private int num; // La position de la pile dans ClassEnvironnement.classes
    private ClassGui gui;

	private static final long serialVersionUID = 7575757575757575757L;	

    public ClassPile(ClassGui gui, int num) {
		super();
		this.gui = gui;
		this.num = num;
	
		initProperties();
		addMouseListener(this);
    }
	
    private void initProperties() {
		setBackground(TablePanel.BG_COLOR);
		// For debugging
		//setBackground(BG_COLOR);
		// GBL //
		setLayout(new GridBagLayout());
		// Inner components //
		/** JE2022: momentaneously suppressed... panel number
			JLabel pileName = new JLabel(""+num);
			GridBagConstraints pileName_gbc = new GridBagConstraints();
			pileName_gbc.insets = new Insets(0, 0, 0, 30);
			add( pileName, pileName_gbc );
		*/
    }
    
    public void addCard( int c, boolean last ) {
		int i = cards.size();
		ClassCard card = new ClassCard( gui, c, i );
		cards.add( card );
		setCardConstraint( card, last );
    }

    protected void setCardConstraint( ClassCard card, boolean last ) {
		GridBagConstraints card_gbc = new GridBagConstraints();
		card_gbc.gridx = cards.size(); //-i = 0 put them vertically!
		card_gbc.insets = new Insets(0, 0, 0, last?0:-(ClassCard.INIT_CARD_SIZE/2) );
		add( card, card_gbc );
    }

    public void addCards( CardSet cardSet ) {
		// Reset the constraint on the last card so that it receives others
		if ( cards.size() > 0 ) {
			ClassCard crd = cards.lastElement();
			remove( crd );
			setCardConstraint( crd, false );
		}
		// Create the new cards to be added
		for ( int card = cardSet.nextSetBit(0); card >= 0; card = cardSet.nextSetBit(card+1)) {
			addCard( card, (cardSet.nextSetBit(card+1)<0) );
		}
		// reorder the vertical component of cards
		int i = getComponentCount();
		for ( Component comp: getComponents() ) {
			i--;
			setComponentZOrder( comp, i );
		}
    }

    public int raiseCard( ClassCard card ) {
		int zorder = getComponentZOrder( card );
		setComponentZOrder( card, 0 );
		repaint();
		return zorder;
    }

    public int replaceCard( ClassCard card, int zorder ) {
		setComponentZOrder( card, zorder );
		repaint();
		return zorder;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public int getNum() {
	return num;
    }

}

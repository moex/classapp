/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Vector;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
    
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.CardSet;

/**
 * The only purpose of this class is to create a clickable component to create new classes.
 */
public class PilesPanel extends JPanel implements MouseListener {
    final static Logger logger = LoggerFactory.getLogger( PilesPanel.class );

    public static final Color BG_COLOR = new Color(50, 200, 50);

    private ClassGui gui;

    private Vector<ClassPile> piles = new Vector<ClassPile>();

	private static final long serialVersionUID = 7575757575757575757L;	

    public PilesPanel( ClassGui gui ) {
		super();
		this.gui = gui;
		
		// Layout
		setLayout( new GridBagLayout() );
		setBackground( BG_COLOR );
		addMouseListener( this );
    }

    public void empty() {
		removeAll();
    }
	@Override
	public void mouseClicked(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
	    /* Si on clique n'importe où sur la table sauf
	     * sur une carte, on crée une nouvelle classe. */
	    CardSet selectedCards = gui.getHandPanel().getSelectedCards();
	    try {
			/* On ne le fait que si au moins une carte est sélectionnée
			 * et que la session n'est pas entrain de faire jouer les bots
			 */
			if ( selectedCards.cardinality()>0 && !gui.getSession().isABotPlaying() )
				gui.getSession().play( -1, selectedCards );
	    } catch ( Exception ex ) {
			ex.printStackTrace();
	    }
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
	    // TODO Auto-generated method stub
	}

	public ClassGui getGui() {
	    return gui;
	}
}

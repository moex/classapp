/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.net.URL;
import java.nio.file.Paths;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import fr.inria.moex.classapp.CardSet;

/****************************************************************************
 * This class contains three implementations of ClassCard with three 
 * different SVG renderers: batik, SVGSalamander and jsvg.
 * They can be activated by changing the comments (there is no need to ship
 * three libraries for the same purpose... 
 ****************************************************************************/

/**
 * JE2023: can't we have a cache here?
 */

/* -- Batik */
import org.apache.batik.swing.JSVGCanvas;

/* --jsvg (works well, just a little less well than batik)
import com.github.weisj.jsvg.parser.SVGLoader;
import com.github.weisj.jsvg.SVGDocument;
import com.github.weisj.jsvg.attributes.ViewBox;
import com.github.weisj.jsvg.attributes.font.SVGFont;
import com.github.weisj.jsvg.geometry.size.FloatSize;
import com.github.weisj.jsvg.geometry.size.MeasureContext;
import com.github.weisj.jsvg.nodes.SVG;
import com.github.weisj.jsvg.renderer.RenderContext;
 */
/* --salamander (Works reasonably well,
 but with bad display of filling 
 cannot distinguish full or hatched)
import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGException;
import com.kitfox.svg.SVGUniverse;
*/
/* --salamander + jsvg
import java.util.Map;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Image;
import static java.awt.RenderingHints.*;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassCard
    /* --salamander + jsvg
    extends JComponent
    */
    /* -- Batik */
    extends JSVGCanvas
    implements MouseListener {
    final static Logger logger = LoggerFactory.getLogger( ClassCard.class );

    public static final Border DEFAULT_BORDER = BorderFactory.createLineBorder(Color.BLACK, 1);
    public static final Border OVERED_BORDER = BorderFactory.createSoftBevelBorder(BevelBorder.RAISED, Color.YELLOW, Color.BLACK);
    public static final Border SELECTED_BORDER = BorderFactory.createLineBorder(Color.RED, 2);

    public static final int INIT_CARD_SIZE = 90;

    private ClassGui gui;
    private boolean selected = false;
    private int num;
    public int pos; // position in a queue

	private static final long serialVersionUID = 7575757575757575757L;	

    public ClassCard( ClassGui gui, int num, int p ) {
		this( gui, num );
		pos = p;
    }
    
    public ClassCard( ClassGui gui, int num ) {
		this.num = num;
		this.gui = gui;
	
		// JE202305: we observe that this is likely wrong...
		// It should rather be number-filling-shape-color
		// To check and see impact
		String filename = "card";
		filename += num % 3; // Number
		filename += (num/3) % 3; // Filling
		filename += (num/9) % 3; // Shape
		filename += (num/27) % 3; // Color
	
		loadFile( filename );
		setBorder( DEFAULT_BORDER );
		
		listen();
    }

    public ClassCard( ClassGui gui, int[] samediff ) {
		this.gui = gui;
		num = -1; // a class...
		
		String filename = "card";
		for ( int i=4-1; i >= 0; i-- ) {
			if ( samediff[i] == -1 ) filename += "+";
			else filename += samediff[i];
		}
	
		loadFile( filename );
		setBorder( DEFAULT_BORDER );
	
		listen();
    }

    /* --Batik */
    private void loadFile( String filename ) {
		String path = "images/svg/";
		// read in jar...
		try {
			setURI( getClass().getClassLoader().getResource( path+filename+".svg" ).toString() );
		} catch ( Exception ex ) {
			logger.error( "Cannot find file "+filename+".svg" );
			ex.printStackTrace();
			System.exit( -1 );
		}
    }

    /* --salamander + jsvg
    private Image img;

   public final static Map<Object, Object> RENDERING_HINTS = Map.of(
								     KEY_ANTIALIASING,
								     VALUE_ANTIALIAS_ON,
								     KEY_ALPHA_INTERPOLATION,
								     VALUE_ALPHA_INTERPOLATION_QUALITY,
								     KEY_COLOR_RENDERING,
								     VALUE_COLOR_RENDER_QUALITY,
								     KEY_DITHERING,
								     VALUE_DITHER_DISABLE,
								     KEY_FRACTIONALMETRICS,
								     VALUE_FRACTIONALMETRICS_ON,
								     KEY_INTERPOLATION,
								     VALUE_INTERPOLATION_BICUBIC,
								     KEY_RENDERING,
								     VALUE_RENDER_QUALITY,
								     KEY_STROKE_CONTROL,
								     VALUE_STROKE_PURE,
								     KEY_TEXT_ANTIALIASING,
								     VALUE_TEXT_ANTIALIAS_ON
								     );
     */
    /* --jsvg
    private void loadFile( String filename ) {
	try {
	    img = rasterize( filename, getPreferredSize() );
	} catch ( Exception ex ) {
	    logger.error( "Cannot find file "+filename+".svg" );
	    ex.printStackTrace();
	    System.exit( -1 );
	}
    }
    
    public SVGDocument loadDiagram( final String filename ) throws Exception {
	String path = "images/svg/";
	final var url = new URL( getClass().getClassLoader().getResource( path+filename+".svg" ).toString() );
	final SVGLoader loader = new SVGLoader();
	return loader.load( url );
    }

    public Image rasterize( final String filename, final Dimension dstDim ) throws Exception {
	final var diagram = loadDiagram( filename );
	final var image = new BufferedImage( INIT_CARD_SIZE, INIT_CARD_SIZE, TYPE_INT_ARGB );
	
	final var g = image.createGraphics();
	g.setRenderingHints( RENDERING_HINTS );

	diagram.render( this, g, new ViewBox( INIT_CARD_SIZE, INIT_CARD_SIZE ) );
	g.dispose();
	
	return image;
    }
 */

    /* --salamander
    private final static SVGUniverse sRenderer = new SVGUniverse();
    
    private void loadFile( String filename ) {
	try {
	    img = rasterize( filename, getPreferredSize() );
	} catch ( Exception ex ) {
	    logger.error( "Cannot find file "+filename+".svg" );
	    ex.printStackTrace();
	    System.exit( -1 );
	}
    }
    
    public SVGDiagram loadDiagram( final String filename ) throws Exception {
	String path = "images/svg/";
	final var url = new URL( getClass().getClassLoader().getResource( path+filename+".svg" ).toString() );
	final var uri = sRenderer.loadSVG( url );
	final var diagram = sRenderer.getDiagram( uri );
	diagram.setIgnoringClipHeuristic( true );
	return diagram;
    }

    public Image rasterize( final String filename, final Dimension dstDim ) throws Exception, SVGException {
	final var diagram = loadDiagram( filename );
	final var wDiagram = diagram.getWidth();
	final var hDiagram = diagram.getHeight();
	final var srcDim = new Dimension( (int) wDiagram, (int) hDiagram );
	
	final var scaled = fit( srcDim, dstDim );
	final var wScaled = (int) scaled.getWidth();
	final var hScaled = (int) scaled.getHeight();
	
	final var image = new BufferedImage( wScaled, hScaled, TYPE_INT_ARGB );
	
	final var g = image.createGraphics();
	g.setRenderingHints( RENDERING_HINTS );
	
	final var transform = g.getTransform();
	transform.setToScale( wScaled / wDiagram, hScaled / hDiagram );
	
	g.setTransform( transform );
	diagram.render( g );
	g.dispose();
	
	return image;
    }

    private Dimension fit( final Dimension src, final Dimension dst ) {
	final var srcWidth = src.getWidth();
	final var srcHeight = src.getHeight();

	// Determine the ratio that will have the best fit.
	final var ratio = Math.min(
				   dst.getWidth() / srcWidth, dst.getHeight() / srcHeight
				   );
	
	// Scale both dimensions with respect to the best fit ratio.
	return new Dimension( (int) (srcWidth * ratio), (int) (srcHeight * ratio) );
    }
	*/

    /* --jsvg + salamander
    @Override
    protected void paintComponent( final Graphics graphics ) {
	super.paintComponent( graphics );

	final var g = (Graphics2D) graphics.create();
	g.drawImage( img, 0, 0, this );
    }
    */
    
    /**
     * Below applicable to all renderers
     */

    @Override
    public Dimension getPreferredSize() {
		// TODO Trouver un moyen d'adapter la taille des cartes � l'espace dispo
		return new Dimension( INIT_CARD_SIZE, INIT_CARD_SIZE );
    }

    public boolean isSelected() {
		return selected;
    }

    public int getNum() {
		return num;
    }

    /**
     * Implementation of the mouse listener
     */

    public void stopListening() {
		removeMouseListener(this);
    }

    public void listen() {
		addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    public void toggleSelection() {
		selected = !selected;
		setBorder( selected?SELECTED_BORDER:DEFAULT_BORDER );
    }
				  
    @Override
    public void mousePressed( MouseEvent e ) {
		if ( getParent() instanceof CardHand ) { // Selected card in hand
			toggleSelection();
			((CardHand)getParent()).toggleCardSelection( this );
		} else if ( getParent() instanceof ClassPile ) { // Selected pile to play
			CardSet selectedCards = gui.getHandPanel().getSelectedCards();
			try {
				/* On ne le fait que si au moins une carte est s�lectionn�e
				 * et que la session n'est pas entrain de faire jouer les bots
				 */
				if ( selectedCards.cardinality()>0 && !gui.getSession().isABotPlaying() )
					gui.getSession().play( ((ClassPile)getParent()).getNum(), selectedCards);
			} catch ( Exception ex ) {
				ex.printStackTrace();
			}
		}
    }
    @Override
    public void mouseReleased(MouseEvent e) {}
    
    int zorder;
    
    @Override
    public void mouseEntered(MouseEvent e) {
		if (getParent() instanceof HandPanel) { 
			setBorder(OVERED_BORDER);
		} else if (getParent() instanceof ClassPile) {
			zorder = ((ClassPile)getParent()).raiseCard( this );
		}
    }
    @Override
    public void mouseExited(MouseEvent e) {
		if (getParent() instanceof HandPanel) {
			if ( selected ) {
				setBorder( SELECTED_BORDER );
			} else {
				setBorder( DEFAULT_BORDER );
			}
		} else if (getParent() instanceof ClassPile) { 
			((ClassPile)getParent()).replaceCard( this, zorder );
		}
    }
    
}

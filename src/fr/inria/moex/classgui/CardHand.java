/*
 * Copyright (C) INRIA, 2023
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Vector;
import java.util.Set;
import java.util.HashSet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.CardSet;
import fr.inria.moex.classapp.ClassGame;

public class CardHand extends JPanel {
    final static Logger logger = LoggerFactory.getLogger( CardHand.class );

    public static final Color BG_COLOR = new Color(50, 50, 50);
    public static final Insets DEFAULT_CARD_INSETS = new Insets(10, 5, 10, 5);
    public static final Insets SELECTED_CARD_INSETS = new Insets(-20, 5, 10, 5);

    HandPanel panel = null;
    
	private static final long serialVersionUID = 7575757575757575757L;	

    public CardHand( HandPanel p ) {
		panel = p;
		setLayout( new GridBagLayout() );
		setBackground( BG_COLOR ); //Color.RED
    }

    public void toggleCardSelection( ClassCard card ) {
		GridBagLayout layout = (GridBagLayout)getLayout();
		GridBagConstraints card_gbc = layout.getConstraints( card );
		card_gbc.insets = card.isSelected() ? SELECTED_CARD_INSETS : DEFAULT_CARD_INSETS;
		layout.setConstraints( card, card_gbc );
    }    
}

/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Map.Entry;
import java.util.Properties;
import java.net.URL;
import java.net.URI;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;

import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.InputMap;
import javax.swing.ActionMap;
import javax.swing.KeyStroke;
import javax.swing.AbstractAction;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.CardSet;
import fr.inria.moex.classapp.ClassEnvironment;

public class ClassGui extends PlayClass {
	final static Logger logger = LoggerFactory.getLogger( ClassGui.class );

	private static final String DISPATCH = "dispatch";

	private static final EmptyBorder border = new EmptyBorder( 8, 8, 8, 8 );

	private JFrame frame;
	private GridBagLayout frame_gbl;
	private InfoPanel infoPanel;
	private TablePanel tablePanel;
	private HandPanel handPanel;

	private ClassGraphicSession session;

	private int nbGames = 1;

	private static final long serialVersionUID = 7575757575757575757L;	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater( new Runnable() {
			public void run() {
				try {
					new ClassGui().run(args);
				} catch ( Exception ex ) {
					ex.printStackTrace();
				}
			}
		});
	}

	/* Called by super.run() */
	@Override
	public void process() throws Exception {
		createFrame();
		frame.setVisible( true );
		play();
	}

	/**
	 * Create the application.
	 */
	public ClassGui() {}

	public void playAgain() throws Exception {
		nbGames++;
		resetGui();
		play();
	}

	public void play() throws Exception {
		// This is the content of process...
		session = new ClassGraphicSession( this );
		logger.debug( "Here is the current session: {}", session );
		session.init( parameters );
		session.initSession();
		infoPanel.initProperties( session );

		// Add initial class to GUI (class 0)
		int cl = tablePanel.addClassToTable();
		tablePanel.addCardsToClass( cl, session.getEnvironment().getCards( 0 ) );

		// JE2022: Does not update the gui???
		session.play( -2, null );
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void createFrame() throws Exception {
		// Initialisation des panels contenants d'autres panels
		frame = new JFrame( PlayClass.getString( "name" ) );
		// Default dimension: a quarter of the screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screenSize.width/2, screenSize.height/2);
		// Open full screen
		frame.setExtendedState( frame.getExtendedState() | JFrame.MAXIMIZED_BOTH );
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_gbl = new GridBagLayout();
		frame.getContentPane().setLayout(frame_gbl);

		// Add subpanels
		// JE2022: I think that this is a problem that the constraints (and add) are provide from the inside.
		// JE2023: Yes...
		infoPanel = new InfoPanel( this );
		tablePanel = new TablePanel( this );
		handPanel = new HandPanel( this );

		// Add key bindings
		JLabel phantomLabel = new JLabel();
		InputMap imap = phantomLabel.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
		ActionMap amap = phantomLabel.getActionMap();
		imap.put( KeyStroke.getKeyStroke('i'), DISPATCH ); // About/info
		imap.put( KeyStroke.getKeyStroke('a'), DISPATCH ); // About
		imap.put( KeyStroke.getKeyStroke('b'), DISPATCH ); // (T)Board
		imap.put( KeyStroke.getKeyStroke('c'), DISPATCH ); // (T)Cheat
		imap.put( KeyStroke.getKeyStroke('h'), DISPATCH ); // Help
		imap.put( KeyStroke.getKeyStroke('s'), DISPATCH ); // Settings
		imap.put( KeyStroke.getKeyStroke('q'), DISPATCH ); // Quit
		imap.put( KeyStroke.getKeyStroke('T' ), DISPATCH ); // Teacher mode
		amap.put( DISPATCH, new DispatchAction( this ) );
		frame.add( phantomLabel );
	}

	public void resetGui() {
		if ( frame != null ) {
			infoPanel.empty();
			tablePanel.empty();
			handPanel.empty();
			frame.repaint();
		}
		if ( loadedClassif ) {
			classifPanel.empty();
			loadedClassif = false;
		}			
	}

	public void updateGui( ClassGame lastTurn ) {
		handPanel.updateCards( lastTurn );
		tablePanel.updateTable( lastTurn );
		infoPanel.updateInfos( lastTurn );
		frame.repaint();
	}

	public ClassGraphicSession getSession() {
		return session;
	}
	public JFrame getFrame() {
		return frame;
	}
	public TablePanel getTablePanel() {
		return tablePanel;
	}
	public HandPanel getHandPanel() {
		return handPanel;
	}

	private boolean teacherMode = false;

	private class DispatchAction extends AbstractAction {

		ClassGui gui;

		private static final long serialVersionUID = 7575757575757575757L;	

		DispatchAction( ClassGui g ) {
			super();
			gui = g;
		}

		@Override
		public void actionPerformed( ActionEvent e ) {
			char c = e.getActionCommand().charAt(0);
			//System.err.println( "YES "+c+" HAS BEEN HIT BY "+e.getSource() );
			if ( c == 'i' || c == 'a' ) {        // info panel
				gui.infoFrame();
			} else if ( c == 's' ) { // setting panel
				gui.settingFrame();
			} else if ( c == 'b' && teacherMode ) { // board panel
				gui.classifFrame();
			} else if ( c == 'h' ) { // help panel
				gui.helpFrame();
			} else if ( c == 'c' && teacherMode ) { // cheat panel!
				gui.cheatFrame();
			} else if ( c == 'z' ) {
			} else if ( c == 'q' ) { // quit
				System.exit( -1 );
			} else if ( c == 'T' ) {
				teacherMode = !teacherMode;
			} else if ( c == 't' && (e.getModifiers() & java.awt.event.InputEvent.CTRL_DOWN_MASK ) != 0 ) {
				teacherMode = !teacherMode;
			}
		}
	}

	JDialog infoBox = null;
	JDialog helpBox = null;

	public void infoFrame() {
		if ( infoBox == null ) infoBox = createInfoFrame( "infoname", "info" );
		if ( infoBox.isVisible() ) { // does not really works... should go in the dialog
			infoBox.setVisible( false );
		} else {
			infoBox.setVisible( true );
		}
	}

	public void helpFrame() {
		if ( helpBox == null ) helpBox = openModal( "helpname", "help" );
		if ( helpBox.isVisible() ) {
			helpBox.setVisible( false );
		} else {
			helpBox.setVisible( true );
		}
	}

	public JDialog createInfoFrame( String name, String filename ) {
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( "helpname" ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setBackground( Color.WHITE );
		dialogContainer.setLayout( new BoxLayout( dialogContainer, BoxLayout.Y_AXIS ) ); //BorderLayout
		// Logo
		//URL lurl = getClass().getClassLoader().getResource( "images/png/class-game-logo.png" );
		URL lurl = getClass().getClassLoader().getResource( "images/png/class-game-name.png" );
		JLabel logo = new JLabel( new ImageIcon( lurl ) );
		logo.setAlignmentX( Component.CENTER_ALIGNMENT );
		dialogContainer.add( logo );
		// URL
		JButton uButton = new JButton( "https://moex.inria.fr/mediation/class/" );
		uButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				// Launch it
				try {
					if ( Desktop.isDesktopSupported() &&
							Desktop.getDesktop().isSupported(Desktop.Action.BROWSE) ) {
						Desktop.getDesktop().browse(new URI("https://moex.inria.fr/mediation/class/"));
					}
				} catch ( Exception ex ) {};
			} } );
		uButton.setAlignmentX( Component.CENTER_ALIGNMENT );
		uButton.setOpaque(false);
		uButton.setContentAreaFilled(false);
		uButton.setBorderPainted(false);
		uButton.setFocusPainted(false);
		uButton.setForeground(Color.BLUE);
		dialogContainer.add( uButton );
		// Content
		String content = PlayClass.getHTMLFile( getClass(), "/fr/inria/moex/classgui/resources/", filename );
		JLabel label = new JLabel( content );
		label.setBorder( border );
		//label.setFont( new Font("Verdana",1,20) );
		label.setAlignmentX( Component.CENTER_ALIGNMENT );
		dialogContainer.add( label );
		// QRCode
		URL qurl = getClass().getClassLoader().getResource( "images/png/class-game-qrcode.png" );
		JLabel qr = new JLabel( new ImageIcon( qurl ) );	
		qr.setAlignmentX( Component.CENTER_ALIGNMENT );
		dialogContainer.add( qr );
		// Button
		JButton qButton = new JButton( PlayClass.getString("close") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				modalDialog.setVisible(false);
				modalDialog.dispose();
			}
		});
		qButton.setAlignmentX( Component.CENTER_ALIGNMENT );
		dialogContainer.add( qButton );
		// Better put the compilation date!
		JLabel dtlab1 = new JLabel( LocalDateTime.now().format( DateTimeFormatter.ISO_LOCAL_DATE ) );
		dtlab1.setAlignmentX( Component.CENTER_ALIGNMENT );
		dialogContainer.add( dtlab1 );
		Package pkg = this.getClass().getPackage();
		if ( pkg != null ) {
			//JLabel dtlab = new JLabel( pkg.getAttribute( "Built-Date" ) );
			//dtlab.setAlignmentX( Component.CENTER_ALIGNMENT );
			//dialogContainer.add( dtlab );
			JLabel ghash = new JLabel( pkg.getImplementationVersion() );
			ghash.setAlignmentX( Component.CENTER_ALIGNMENT );
			dialogContainer.add( ghash );
		}
		// End

		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		return modalDialog;
	}

	public JDialog openModal( String name, String filename ) {
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( name ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout( new BorderLayout() ); //new BoxLayout( dialogContainer, BoxLayout.Y_AXIS ) ); //BorderLayout
		// Content
		String content = PlayClass.getHTMLFile( getClass(),  "/fr/inria/moex/classgui/resources/", filename );
		JLabel label = new JLabel( content );
		label.setBorder( border );
		//label.setFont( new Font("Verdana",1,20) );
		dialogContainer.add( label ); //, BorderLayout.CENTER
		// Button
		JPanel panel1 = new JPanel();
		panel1.setLayout( new FlowLayout() );
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("close") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				modalDialog.setVisible(false);
				modalDialog.dispose();
			}
		});
		panel1.add(qButton);
		// End

		dialogContainer.add(panel1, BorderLayout.SOUTH);

		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		return modalDialog;
	}

	JDialog classifBox = null;
	ClassificationPanel classifPanel;
	private boolean loadedClassif = false;

	public void classifFrame() {
		if ( classifBox == null ) {
			classifBox = createClassifFrame( "classifname" );
		}
		if ( !loadedClassif ) {
			classifPanel.loadClassification( session.getClassification() );
			loadedClassif = true;
			redrawDialog( classifBox );
		}
		
		if ( classifBox.isVisible() ) { // does not really works... should go in the dialog
			classifBox.setVisible( false );
		} else {
			classifBox.setVisible( true );
		}
	}
	
	public JDialog createClassifFrame( String name ) {
		// Creates the frame
		final JDialog modalDialog = new JDialog( frame, PlayClass.getString( name ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout( new BorderLayout() );

		classifPanel = new ClassificationPanel( this );

		JScrollPane classifjsp = new JScrollPane( classifPanel );
		dialogContainer.add( classifjsp );

		// Button
		JPanel panel1 = new JPanel();
		panel1.setLayout( new FlowLayout() );
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("close") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				modalDialog.setVisible(false);
				modalDialog.dispose();
			}
		});
		panel1.add(qButton);

		dialogContainer.add( panel1, BorderLayout.SOUTH );
		return modalDialog;
	}

	public void redrawDialog( JDialog dialog ) {
		dialog.pack();
		dialog.repaint();
		dialog.setLocationRelativeTo( frame );
	}

	public void cheatFrame() {
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( "cheatname" ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout( new BorderLayout() );

		Font font = new Font( "SansSerif", Font.PLAIN, 24 );
		Insets ins = new Insets( 5, 5, 5, 5 );

		// A grid panel:
		JPanel gPanel = new JPanel();
		gPanel.setLayout( new GridBagLayout() );
		gPanel.setBorder( border );
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		// Layout
		gPanel.setLayout(new GridBagLayout());
		gPanel.setBackground( Color.YELLOW );

		ClassEnvironment env = session.getEnvironment();
		int nbClasses = env.nbClasses();

		// The hand
		CardSet hand = handPanel.getHand();
		int xpos = 1;
		int ypos = nbClasses;
		if ( hand != null ) {
			for ( int i = hand.nextSetBit(0); i >= 0; i = hand.nextSetBit( i+1 )) {
				ClassCard card = new ClassCard( this, i );
				GridBagConstraints card_gbc = new GridBagConstraints();
				card_gbc.gridx = xpos;
				card_gbc.gridy = ypos;
				card_gbc.insets = ins;
				gPanel.add( card, card_gbc );
				xpos++;
			}
		}

		Dimension size = new Dimension( ClassCard.INIT_CARD_SIZE, ClassCard.INIT_CARD_SIZE );
		// For each class... starting from...
		for ( int cl = nbClasses-1; cl >= 0 ; cl-- ) {
			xpos = 0;
			ypos--;
			CardSet wrong = env.getWrongs( cl );
			int[] samediff = env.getSameDiff( cl );
			// Display class rank
			JLabel cllab = new JLabel( cl+":", JLabel.CENTER );
			cllab.setPreferredSize( size );
			cllab.setFont( font );
			GridBagConstraints cl_gbc = new GridBagConstraints();
			cl_gbc.gridx = xpos;
			cl_gbc.gridy = ypos;
			gPanel.add( cllab, cl_gbc );

			// Display class belonging
			for ( int i = hand.nextSetBit(0); i >= 0; i = hand.nextSetBit(i+1)) {
				xpos++;
				// I must determine the distance
				JLabel dist = new JLabel( ""+env.incCount( i, samediff ), JLabel.CENTER );
				dist.setPreferredSize( size );
				dist.setFont( font );
				// Previous...
				GridBagConstraints dist_gbc = new GridBagConstraints();
				dist_gbc.gridx = xpos;
				dist_gbc.gridy = ypos;
				gPanel.add( dist, dist_gbc );
				if ( wrong.get( i ) ) { // Crossed JLabel then
					//dist.setForeground(Color.RED);
					JPanel panel = new javax.swing.JPanel() {
						@Override
						protected void paintComponent(Graphics g) {
							((Graphics2D)g).setStroke( new BasicStroke(3) );
							g.drawLine( ClassCard.INIT_CARD_SIZE, 0, 0, ClassCard.INIT_CARD_SIZE );
						}
					};
					panel.setPreferredSize( size );
					GridBagConstraints panel_gbc = new GridBagConstraints();
					panel_gbc.gridx = xpos;
					panel_gbc.gridy = ypos;
					gPanel.add( panel, panel_gbc );
				}
			}
			xpos++;
			// Display class pattern
			ClassCard clcard = new ClassCard( this, samediff );
			GridBagConstraints clcard_gbc = new GridBagConstraints();
			clcard_gbc.gridx = xpos;
			clcard_gbc.gridy = ypos;
			clcard_gbc.insets = ins;
			gPanel.add( clcard, clcard_gbc );
		}

		dialogContainer.add( gPanel, BorderLayout.CENTER);

		// The final button
		JPanel panel1 = new JPanel();
		panel1.setLayout( new FlowLayout() );
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("close") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				modalDialog.setVisible(false);
				modalDialog.dispose();
			}
		});
		panel1.add(qButton);

		dialogContainer.add(panel1, BorderLayout.SOUTH);

		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		modalDialog.setVisible(true);
	}

	public void endSession( int player ) {
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( "endname" ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout(new BorderLayout());
		JLabel label;
		if ( player == -1 ) {
			label = new JLabel( "<html><div style=\"text-align: center; font-size: 150%;\">"+PlayClass.getString("ywon")+"</div></html>" );
		} else {
			label = new JLabel( "<html><div style=\"text-align: center; font-size: 150%;\">"+PlayClass.getString("player")+player+PlayClass.getString("won")+".</div></html>" );
		}
		label.setBorder( border );
		//label.setFont( new Font("Verdana",1,20) );
		dialogContainer.add( label, BorderLayout.CENTER);
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("quit") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panel1.add( qButton );
		JButton rButton = new JButton( PlayClass.getString("replay")+" ("+nbGames+")" );
		rButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				try {
					session = null; // JE2022 useless
					playAgain();
				} catch (Exception ex) {
					ex.printStackTrace();
					System.exit(-1);
				} finally {
					modalDialog.setVisible( false );
					modalDialog.dispose();
				}
			}
		});
		panel1.add(rButton);
		/*
		  JButton gButton = new JButton( PlayClass.getString("guess") );
		  gButton.addActionListener(new ActionListener() {
		  @Override
		  public void actionPerformed(ActionEvent e) {
		  }
		  });
		  panel1.add(gButton);
		 */
		JButton sButton = new JButton( PlayClass.getString("see") );
		sButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				classifFrame();
			}
		});
		panel1.add(sButton);
		dialogContainer.add(panel1, BorderLayout.SOUTH);

		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		modalDialog.setVisible(true);
	}

	public void showErrorWarning( Exception ex ) {
		ex.printStackTrace();
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( "errorname" ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout(new BorderLayout());
		JLabel label = new JLabel( "<html><div style=\"text-align: center; font-size: 150%;\">"+ex.getMessage()+"</html>" );
		label.setBorder( border );
		//label.setFont( new Font("Verdana",1,20) );
		dialogContainer.add( label, BorderLayout.CENTER);
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("quit") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panel1.add(qButton);
		JButton rButton = new JButton( PlayClass.getString("replay")+" ("+nbGames+")" );
		rButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				try {
					session = null; // JE2022 useless
					playAgain();
				} catch (Exception ex) {
					ex.printStackTrace();
					System.exit(-1);
				} finally {
					modalDialog.setVisible( false );
					modalDialog.dispose();
				}
			}
		});
		panel1.add(rButton);

		dialogContainer.add(panel1, BorderLayout.SOUTH);

		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		modalDialog.setVisible(true);
	}


	/**
	 * JE2022: Here is where we are:
	 * (1) What is below works... but only changes the existing at launch time __init__ parameters
	 *     It works because the parameters originate from here (parameters)
	 * (2) session.getParameters() is actually more sophisticated, gathering parameters from everywhere
	 *     For it to work would also require a session.setParameters() which would set them at the right place...
	 *     This is perfectly possible.
	 * (3) Another option would also be to 'allow' such parameters at command-line.
	 */
	// Here I need a set of widgets to fix the parameters
	public void settingFrame() {
		final JDialog modalDialog = new JDialog(frame, PlayClass.getString( "settingname" ), 
				Dialog.ModalityType.DOCUMENT_MODAL);
		Container dialogContainer = modalDialog.getContentPane();
		dialogContainer.setLayout(new BorderLayout()); 

		// Get parameters from session
		//Properties params = session.getParameters();
		Properties params = parameters;

		// Display them
		JPanel gPanel = new JPanel();
		gPanel.setLayout( new GridBagLayout() );
		gPanel.setBorder( border );
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		JTextField[] fields = new JTextField[params.size()];
		int i = 0;
		for ( Entry<Object, Object> entry : params.entrySet() ) {
			c.gridy = i;
			JLabel lab = new JLabel( entry.getKey()+": ", JLabel.RIGHT );
			lab.setPreferredSize( new Dimension(170, 25) );
			c.gridx = 0;
			gPanel.add( lab, c );
			JTextField val = new JTextField( (String)entry.getValue() );
			val.setPreferredSize( new Dimension(170, 25) );
			c.gridx = 1;
			fields[i] = val;
			gPanel.add( val, c );
			i++;
		}

		dialogContainer.add( gPanel, BorderLayout.CENTER);

		// Second component: the actions
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder( border );
		JButton qButton = new JButton( PlayClass.getString("cancel") );
		qButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modalDialog.setVisible(false);
				modalDialog.dispose();
			}
		});
		panel1.add(qButton);
		JButton rButton = new JButton( PlayClass.getString("apply") );
		rButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				try {
					int i = 0; // Order independent?
					for ( Entry<Object, Object> entry : parameters.entrySet() ) {
						String val = fields[i].getText();
						if ( !val.equals( entry.getValue().toString() ) ) {
							parameters.setProperty( entry.getKey().toString(), val );
						}
						i++;
					}
					playAgain();
				} catch (Exception ex) {
					ex.printStackTrace();
					System.exit(-1);
				} finally {
					modalDialog.setVisible( false );
					modalDialog.dispose();
				}
			}
		});
		panel1.add(rButton);
		dialogContainer.add(panel1, BorderLayout.SOUTH);

		// Go ahead
		modalDialog.pack();
		modalDialog.repaint();
		modalDialog.setLocationRelativeTo( frame );
		modalDialog.setVisible(true);
	}

}


/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.ClassPlayer;

public class PlayerInfoPanel extends JPanel implements MouseListener {
    final static Logger logger = LoggerFactory.getLogger( PlayerInfoPanel.class );
	
    public static final Border DEFAULT_BORDER = BorderFactory.createLineBorder(Color.BLACK, 0);
    public static final Border CURRENT_PLAYER_BORDER = BorderFactory.createLineBorder(Color.BLACK, 5);

    private JLabel lbl;
    private ClassPlayer agent;
    private ClassGame lastTurn = null;
    private TurnPanel turnPanel = null;
    private boolean isTurnPanelOutdated = true;
	
	private static final long serialVersionUID = 7575757575757575757L;	

    public PlayerInfoPanel( ClassPlayer agent ) {
		super();
		this.agent = agent;
		lbl = new JLabel("");
	
		updateInfos();
		setCurrentPlayer( false );
		add(lbl);

		listen();
    }

    public void updateLastTurn( ClassGame turn ) {
		lastTurn = turn;
		isTurnPanelOutdated = true;
    }

    public void updateInfos() {
		if ( agent.isInteractive() ) {
			lbl.setText("<html><center>"+PlayClass.getString("player")+agent.getId()+" ("+PlayClass.getString("you")+")<br /><span style=\"font-size: 200%\">"+agent.nbCards()+"</span><br />"+PlayClass.getString("cartes")+"</center></html>");
		} else if ( agent.isKnower() ) {
			lbl.setText("<html><center>"+PlayClass.getString("player")+agent.getId()+" ("+PlayClass.getString("knower")+")<br /><span style=\"font-size: 200%\">"+agent.nbCards()+"</span><br />"+PlayClass.getString("cartes")+"</center></html>");
		} else {
			lbl.setText("<html><center>"+PlayClass.getString("player")+agent.getId()+"<br /><span style=\"font-size: 200%\">"+agent.nbCards()+"</span><br />"+PlayClass.getString("cartes")+"</center></html>");
		}
    }
    
    public ClassPlayer getAgent() {
		return agent;
    }
	
    public void empty() {
		isTurnPanelOutdated = true;
		turnPanel = null;
		lastTurn = null;
    }
    
    public void setCurrentPlayer( boolean active ) {
		// TODO A Utiliser
		if (active) setBorder(CURRENT_PLAYER_BORDER);
		else setBorder(DEFAULT_BORDER);
    }
	
    /**
     * Implementation of the mouse listener
     */

    public void stopListening() {
		removeMouseListener( this );
    }

    public void listen() {
		addMouseListener( this );
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    private JFrame turnFrame;
    
    @Override
    public void mousePressed(MouseEvent e) {
		if ( lastTurn == null ) return;
		if ( turnFrame == null ) {
			turnFrame = new JFrame();
			turnFrame.setLocationRelativeTo( null );
			turnFrame.setUndecorated( true );
			turnFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
			GridBagLayout frame_gbl = new GridBagLayout();
			turnFrame.getContentPane().setLayout( frame_gbl );
			isTurnPanelOutdated = true;
		}
		if ( isTurnPanelOutdated ) {
			if ( turnPanel == null ) {
				// Ugly, but works... so far
				turnPanel = new TurnPanel( null );
				turnFrame.add( turnPanel );
			}
			turnPanel.displayTurn( lastTurn );
			isTurnPanelOutdated = false;
			turnFrame.pack();
			turnFrame.repaint();
		}
		if ( !turnFrame.isVisible() ) turnFrame.setVisible( true );
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
		if ( turnFrame != null && turnFrame.isVisible() ) {
			turnFrame.setVisible( false );
		}
    }
	
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }   
}

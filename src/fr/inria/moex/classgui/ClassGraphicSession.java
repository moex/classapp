/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Properties;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.ClassEnvironment;
import fr.inria.moex.classapp.ClassGame;
import fr.inria.moex.classapp.CardSet;
import fr.inria.moex.classapp.ClassPlayer;
import fr.inria.moex.classapp.ClassPlayerAgent;
import fr.inria.moex.classapp.ClassSession;
import fr.inria.moex.classapp.PlayClass;

public class ClassGraphicSession extends ClassSession {
    final static Logger logger = LoggerFactory.getLogger( ClassGraphicSession.class );

    /**
     * Delay between play
     */
    private int LAGDELAY = 0*1000; // this is in milliseconds
    //private int LAGDELAY = 10*1000; // this is in milliseconds

    private ClassGui gui;

    public ClassGraphicSession( ClassGui gui ) {
	super();
	setOutput( null );
	this.gui = gui;
    }
	
   @Override
    public void init( Properties params ) throws Exception {
	super.init( params ); // This reads and creates everything
	if ( params.getProperty( "LagDelay" ) != null ) {
	    LAGDELAY = Integer.parseInt( params.getProperty( "LagDelay" ) ) * 1000;
	}
	// With the gui, there is a single interactive player
	// It is not yet ready for just showing the interactions
	INTERACT = 1;
    }

    public void initSession() throws Exception {
	super.initSession();
	iteration = 0;
	currentPlayer = knower; // Le 1er joueur est le knower
    }	

    /**
     * Class used for processing plays with delay.
     * JE2022: This works as expected but seems to cause badly diplayed cards sometimes...
     * To investigate.
     **/
    class WaitingThread extends Thread {

	private int cl;
	private CardSet cards;
	private ClassGraphicSession session;
	
	public WaitingThread( int c, CardSet b, ClassGraphicSession s ) {
	    super();
	    init( c, b, s );
	}

	public void init( int c, CardSet b, ClassGraphicSession s ) {
	    cl = c;
	    cards = b;
	    session = s;
	}

	public void run () {
	    session.playCompCore( cl, cards );
	    try {
		Thread.sleep( LAGDELAY );
	    } catch ( InterruptedException iex ) {
		iex.printStackTrace();
		gui.showErrorWarning( iex );
	    }
	}
    }

    /**
     * The method called by ClassGUI
     * Here it switches between delayed or not
     *
     * @param cl
     * @param cards
     * @throws Exception
     */
    public void play( int cl, CardSet cards ) throws Exception {
	if ( LAGDELAY > 0 ) { // If we use a delay
	    WaitingThread thread = new WaitingThread( cl, cards, this );
	    thread.start();
	} else {
	    playCompCore( cl, cards );
	}
    }

    /* This simply allow to connect the playCore to the rest */
    public void playCompCore( int cl, CardSet cards ) {
	try {
	    ClassGame turn = playCore( cl, cards );
	    if ( currentPlayer.nbCards() == 0 ) {
		gui.updateGui( turn );
		endSession( currentPlayer );
	    } else {
		player++;
		if ( player == numberOfAgents ) player = 0;
		currentPlayer = agents[player];			
		gui.updateGui( turn );
		if ( !currentPlayer.isInteractive() ) {
		    play(-2, null); // Si le joueur suivant est un bot on rappelle la m�thode
		}
	    }
	} catch (Exception ex) { // Typical empty desk exception
	    logger.error( ex.getMessage() ); // printStackTrace is also possible...
	    gui.showErrorWarning( ex );
	}
    }

    /**
     * The method that plays a turn (inspired from ClassSession)
     * Likely a lot of it should return in it...
     * 
     * @param cl
     * @param cards
     * @throws Exception
     */
    public ClassGame playCore( int cl, CardSet cards ) throws Exception {
	iteration++;
	ClassGame turn;
	// Play
	if ( currentPlayer.isInteractive() ) {
	    logger.debug( PlayClass.getString( "inter" ), iteration );
	    lastInteractivePlayer = currentPlayer;
	    turn = new ClassGame( currentPlayer, cards, cl );
	} else {
	    logger.debug( PlayClass.getString( "bot" ), iteration );
	    turn = new ClassGame( currentPlayer );
	    currentPlayer.playGame( turn );
	}
	// Decide
	turnIssue( turn );
	return turn;
    }

    public void endSession( ClassPlayer player ) {
	logger.debug( "====== {} ======", PlayClass.getString( "endgame" ) );
	if ( player.isInteractive() ) {
	    logger.info( PlayClass.getString( "ywon" ) );
	    gui.endSession( -1 );
	} else {
	    logger.info( PlayClass.getString( "pwon" ), player.getId() );
	    gui.endSession( player.getId() );
	}
    }

    /**
     * Parameter management for editing these
     */
    public Properties getParameters() {
	//Properties params = super.getParameters();
	Properties params = new Properties();
	params.setProperty( "nbAgents", String.valueOf( numberOfAgents ) );
	params.setProperty( "nbInitCards", String.valueOf( NBINITCARDS ) );
	/* JE2022: This is implementation specific
	if ( knower != null ) {
	    params.setProperty( "classStrategy", knower.getStrategy() );
	    params.setProperty( "classKnowerStrategy", knower.getKnowerStrategy() );
	    }*/
	if ( env != null ) {
	    params.setProperty( "nbLeaves", String.valueOf( env.numberOfLeaves() ) );
	    params.setProperty( "nbLevels", String.valueOf( env.numberOfLevels() ) );
	}
	//	params.setProperty( "nbRuns",  );
	params.setProperty( "interactive", String.valueOf( INTERACT ) );
	//params.setProperty( "hints", );
	params.setProperty( "LagDelay", String.valueOf( LAGDELAY/1000 ) );
	params.setProperty( "Language", PlayClass.getLocale().getLanguage() );
	return params;
    }

    public boolean isABotPlaying() {
	return !currentPlayer.isInteractive();
    }

    public boolean isActive( ClassPlayer agent ) {
	return currentPlayer.equals(agent);
    }

}

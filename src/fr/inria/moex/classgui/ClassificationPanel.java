/*
 * Copyright (C) Emma Rechon-Reguet, 2023
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.util.Arrays;
import java.util.Hashtable;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;

import fr.inria.moex.classapp.Feature;
import fr.inria.moex.classgui.ClassCard;
import fr.inria.moex.classapp.Classification;

/**
 * Panel to display a classification
 * @author emmar
 *
 */

public class ClassificationPanel extends JPanel {
	private  ClassGui gui;

	private static int WIDTH;
	private static int HEIGHT = 600;
	private static int EDGE = 120;
	private static int CARDSIZE = ClassCard.INIT_CARD_SIZE;
	private static int CARDOFFSET = CARDSIZE/2;

	/**
	 * this variable takes one int per feature
	 * if the feature.s is.are not defined, valueFeat of the feature will be equals to -1
	 * the order of the features : COLOR ; SHAPE ; FILLING ; NUMBER
	 */
	private static final int[] valueFeat = new int[] {-1,-1,-1,-1}; 
	private int[][] coord ;
	private int nbCoord;

	private static final long serialVersionUID = 7575757575757575757L;	

	// JE2023: should eventually go in Classification
	public int[] setFeature( int[] features, Feature feature, int value ) {
		int[] newFeat = Arrays.copyOf(features, features.length);
		switch ( feature ) {
		case COLOR: newFeat[3] = value; break;
		case SHAPE: newFeat[2] = value; break;
		case FILLING: newFeat[1] = value; break;
		case NUMBER: newFeat[0] = value; break;
		}
		return newFeat;
	}

	public ClassificationPanel( ClassGui gui ) {
		this.gui = gui;
		setBackground( Color.orange );
		setLayout( null );
	}

	public void loadClassification( Classification cl ) {
		int[] sz = size( cl );
		WIDTH = sz[0]+sz[1];
		setPreferredSize( new Dimension( WIDTH, HEIGHT ) );

		int nbcoords = cl.nbForks()*4;
		coord = new int [nbcoords][4];
		nbCoord=0;
		display( cl, WIDTH/2-CARDOFFSET, CARDOFFSET/2, valueFeat );
		
		revalidate();
		repaint();
	}

	/**
	 * size() determines the necessary size occupied by each subtree of the classification
	 */
	Hashtable <Classification, int[]> sznTable = new Hashtable<Classification, int[]> ();

	// This function (like sznTable) could just be int instead of int[]
	// Because now the two values are always the same (proof by induction)
	private int[] size( Classification cl ) {
		int[] sz;
		if ( cl.subTree == null ) {
			sz = new int [] {EDGE/2,EDGE/2};
			sznTable.put( cl, new int [] { 0,0 } );
		} else {
			int[] sz0 = size( cl.subTree[0] );
			int[] sz1 = size( cl.subTree[1] );
			int[] sz2 = size( cl.subTree[2] );
			int mm = Math.max( sz0[1], sz2[0] );
			//sz = new int [] { sz0[0]+sz0[1]+sz1[0], sz1[1]+sz2[0]+sz2[1]};
			//sznTable.put( cl, new int [] { sz0[1]+sz1[0], sz1[1]+sz2[0]});
			sz = new int [] { 2*mm+sz1[0], sz1[1]+2*mm };
			sznTable.put( cl, new int [] { mm+sz1[0], sz1[1]+mm });
		}
		return sz;
	}

	/**
	 * display() uses the result of size() to position the classes on the panel
	 */
	private void display( Classification cl, int x, int y, int[] valueFeat ) {
		displayClass( x, y, valueFeat );
		
		//int x0 = x-Math.max(sznTable.get(cl)[0], sznTable.get(cl)[1]);
		//int x2 = x+Math.max(sznTable.get(cl)[0], sznTable.get(cl)[1]);
		int x0 = x - sznTable.get(cl)[0];
		int x2 = x + sznTable.get(cl)[1];
		int y1 = y + EDGE;
		int y0 = y + EDGE/2; // Position of the horizontal edge

		if ( cl.subTree != null ) {
			addCoord( x0, y0, x2, y0 );

			addCoord( x0, y0, x0, y1 );
			display(cl.subTree[0],x0,y1,setFeature( valueFeat, cl.feature, 0));

			addCoord( x, y, x, y1 );
			display(cl.subTree[1],x,y1,setFeature( valueFeat, cl.feature, 1));

			addCoord( x2, y0, x2, y1 );
			display(cl.subTree[2],x2,y1,setFeature( valueFeat, cl.feature, 2));
		}
	}
	
	private void addCoord( int x0, int y0, int x1, int y1 ) {
		coord[nbCoord][0]=x0+CARDOFFSET;
		coord[nbCoord][1]=y0+CARDOFFSET;
		coord[nbCoord][2]=x1+CARDOFFSET;
		coord[nbCoord][3]=y1+CARDOFFSET;
		nbCoord++;
	}

	public void displayClass( int x, int y, int[] valueFeat ) {
		ClassCard card = new ClassCard( gui, valueFeat );
		card.setBounds( x, y, CARDSIZE, CARDSIZE );
		add(card);
	}

	public void empty() {
		removeAll();		
	}
	
	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		for (int i=0;i<nbCoord;i++) {
			g.drawLine(coord[i][0], coord[i][1], coord[i][2], coord[i][3]);
		}
	}

}

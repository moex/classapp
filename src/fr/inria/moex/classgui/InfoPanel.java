/*
 * Copyright (C) Nathan Lufuluabo, 2022
 * Copyright (C) INRIA, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.classgui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.PlayClass;
import fr.inria.moex.classapp.ClassGame;

public class InfoPanel extends JPanel {
    final static Logger logger = LoggerFactory.getLogger( InfoPanel.class );

    private GridBagConstraints gbc;

    private int nbPlayers;
    private Component[] panels;
    private ClassGui gui;

	private static final long serialVersionUID = 7575757575757575757L;	

    public InfoPanel( ClassGui gui ) {
		super();
		this.gui = gui;

		setBackground(Color.BLACK);
		// GBC
		// JE2022: This should be done from the outside!
		gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 2;
		gbc.weightx = 3;
		gbc.fill = GridBagConstraints.BOTH;
		gui.getFrame().add( this, gbc );
    }
	
    public void initProperties( ClassGraphicSession session ) {
		// Inner components
		nbPlayers = session.getNumberOfAgents();
		panels = new Component[nbPlayers];
		setLayout( new GridLayout( panels.length, 1) );
		for ( int i=0; i<nbPlayers; i++ ) {
			PlayerInfoPanel panel = new PlayerInfoPanel( session.getAgents()[i] );
			panel.setBackground( new Color( i%2>0?255-24*i:24*i, 100, 255 ) );
			add(panel, i); // Overriden
		}
    }

    public void empty() {
		for (int i=0; i<nbPlayers; i++) ((PlayerInfoPanel)panels[i]).empty();
		panels = null;
		removeAll();
    }

    public void updateInfos( ClassGame lastTurn ) {
		for (int i=0; i<nbPlayers; i++) {
			PlayerInfoPanel panel = (PlayerInfoPanel)panels[i];
			panel.updateInfos();
			if ( panel.getAgent() == lastTurn.getPlayer() ) {
				panel.updateLastTurn( lastTurn );
			}
			if ( gui.getSession().isActive( panel.getAgent() ) )
				panel.setCurrentPlayer( true );
			else
				panel.setCurrentPlayer( false );
		}
    }
    
    @Override
    public Component add(Component comp, int i) {
		panels[i] = comp;
		return super.add(comp, i);
    }
    public void updateCards() {
		// TODO A faire
    }
    public void updateCurrentPlayer() {
		// TODO A faire
    }
}

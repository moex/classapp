/*
 * Copyright (C) INRIA, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.moex.whisper;

import java.util.Properties;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.moex.classapp.Deck;
import fr.inria.moex.classapp.CardSet;
import fr.inria.moex.classapp.ClassEnvironment;
import fr.inria.moex.classapp.Classification;

public class WhisperGame {
	final static Logger logger = LoggerFactory.getLogger( WhisperGame.class );

	public WhisperGame() {}

	/**
	 * java Classification OPT level leaves
	 * generates a classification board of at most levels and exactly leaves.
	 * OPT:
	 * -e: reach exactly levels
	 * -l: generate LaTeX for the board
	 * -p: compile the LaTeX into PNG
	 */ 
	public static void main(String[] args) {

		// CURRENTLY USELESS
		/*
		// parse command line arguments
		if ( args.length < 2 ) {
			System.err.println( PlayClass.getString( "e6" ) );
			System.exit(1);
		}
		for ( int i=0; i < args.length; i++ ) {
			String arg = args[i].trim();
			if ( arg.equals( "-e") ) {
				
			}
			}*/

		// Do it
		try {
			WhisperGame my = new WhisperGame();
			my.runSequence( 6, 21, false, 7 );
		} catch ( Exception ex ){
			ex.printStackTrace();
		}
	}

	public static Vector<int[]> generateSamples( Classification classif, int nbCards ) throws Exception {
		CardSet cards = new Deck().drawCards( nbCards );
		logger.debug( "Cards drawn: {}", cards );
		// Unfortunately a vector of patterns... we can do better
		// THIS DOES NOT WORK => it always generates the same patterns
		return classif.assignCardClasses( cards );
	}

	public static Classification guessClassification ( Vector<int[]> samples, boolean completion ) {
		return Classification.guessCurrentClassification( samples, completion );		
	}
	
	public static void printClassSets( Vector<int[]> classSets ){
		for ( int[] pattern : classSets ) {
			for ( int i=0; i<4; i++ )
				if ( pattern[i] == -1 ) System.err.print( "-" );
				else System.err.print( pattern[i] );
			System.err.print( "  ");
		}
		System.err.println();
	}

	/**
	 * Points to be elaborated:
	 * - Maybe better to do it recursively if we want a tree?
	 * - add the possibility that agents know the number of classes
	 * - develop a ratio nbCards/nbLeaves
	 */
	// Exceptions are about typechecking
	public void runSequence( final int sequenceLength, int nbCards, final boolean variable, final int nbLeaves ) throws Exception {
		Vector<int[]> classSets = null;
		Classification classif = Classification.genBoard( 4, nbLeaves, false );
		int iteration = 0;
		System.err.println( "Iteration "+iteration+": " );
		classif.printTree();
		System.err.println();
		while ( iteration < sequenceLength ){
			iteration++;
			System.err.println( "Iteration "+iteration+": " );
			if ( variable ) {
				// Update cards in function of the size of the classification
				nbCards = 4;
			}
			classSets = generateSamples( classif, nbCards );
			printClassSets( classSets );
			classif = guessClassification( classSets, false ); // Vector<int[]> patterns [, BitSet mask]
			if ( ! classif.allCertain() ) logger.debug( ">>>> UNCERTAIN classification" );
			if ( ! classif.allSafe() ) logger.debug( ">>>> UNSAFE classification" );
			if ( ! classif.allComplete() ) logger.debug( ">>>> possibly INCOMPLETE classification" );
			classif.printTree();
			System.err.println();
		}
	}
}

# Agent interface for implementing _Class?_ strategies

## Purpose

It is possible to implement your own strategy for playing _Class?_.
For that purpose it suffices to implement a player class and to tell the program to use it.

In principle, the interface to implement is very simple (two methods to implement and, in fact, only one).
In reality, implementing an accurate strategy may be quite complex.
In the end, we suggest that you start with the provided example and try to improve it progressively.

For the time being, it is only proposed to implement the behaviour of the guesser (who do not have access to the used classification).
This may change if requested.


## The interface to be implemented

It can deal with its cards through:

~~~~
public class MyClassPlayer extends AbstractClassPlayer implements ClassPlayer {

    /* To do for playing your turn (this has to be implemented) */
    @Override
    public void playGame( ClassGame game ) throws Exception { ... }

    /* To do when notified of the issue of a turn (this has to be implemented, but may be left empty) */
    @Override
    public void acceptNotice( ClassGame game ) { ... }

    /* To do when receiving new cards (initially, upon creating new classes, as penalty) */
    @Override
    public void receiveCards( CardSet cards ) { super.receiveCards( cards ); ... }

    /* To do when releasing cards upon your successful turn */
    @Override
    public void releaseCards( CardSet crds ) { super.releaseCards( crds ); ... }

}
~~~~


The ```receiveCards``` and ```releaseCards``` methods are launched when new cards are added (at the beginning to create the initial hand, or as penalties for creating a new class or for an incorrect play) or retrieved (a correct play) from the hand of the player.
These methods are already implemented in the ```AbstractClassPlayer``` in order to automatically update the necessary structure of the player.
It is thus not necessary to override them, and advised to call them through super if overriding them.

The ```AbstractClassPlayer``` also implements direct access to the necessary structures, i.e. its hand (set of card) and the set of classes on the table:
~~~~
public interface ClassPlayer {

    /* The set of cards in the hand of the player */
    CardSet getHand();

    /* The id */
    int getId();
    
    /* How many classes are there on the table */
    int getCurrentClassNumber()
    
    /* What are the cards of class cl */
    CardSet getCurrentClassCards( int cl )
}
~~~~

So, they can be consulted when playing.

The ```acceptNotice``` method provides at each turn the information about what has been played, by whom and if it was correct or not.
This belongs to the ```ClassGame``` interface (see below).
A player who pays attention to what happens will certainly have to perform some action here.

Finally, the main method to implement a strategy is the ```playGame``` method.
It receives a ```ClassGame``` object to which the player must set:
* the class in which to play (identified by its index or -1 if a new class has to be created);
* the cards to be played to that class.

This is done with the ```play``` method of the ```ClassGame``` interface.

## The interfaces that can be used

Three interfaces are used for describing what happens during the game.

### ClassGame

```ClassGame``` represents one turn of the game:

~~~~
public interface ClassGame {

    /* Playing cards in class cl (-1 for a new class) */
    public void play( CardSet cards, int cl );

    /* the cards played */
    public CardSet getCards();
    /* the class played (-1 for a new class)*/
    public int getAssigned();
    /* the player id */
    public int getPlayerId()
    /* the correctness or incorrectness of the turn */
    public boolean getIssue();
}
~~~~

### CardSet

```CardSet``` is a set of cards:

~~~~
public interface CardSet extends Iterable<Card> {

    /* Number of available cards */
    final static int NBCARDS;

    /* Number of cards in the set */
    public int size();
    
    /* does the set includes the card */
    public boolean includes( Card card );
    /* does the set includes the cards */
    public boolean includes( CardSet cards );

    /* add a card to a set */
    public void addCard( Card card );
    /* add cards to a set */
    public void addCards( CardSet cards );

}
~~~~

It is possible to iterate on the cards of a ```CardSet``` by:
~~~~
CardSet cs;
for( Card c: cs ) {
   ...
}
~~~~

### Card

```Card``` is just a card.

~~~~
public interface Card {

    /* the card with these values */
    public Card( Feature.Number number, Feature.Filling filling, Feature.Shape shape, Feature.Color color );

    public Feature.Number getNumber();
    public Feature.Filling getFilling();
    public Feature.Shape getShape();
    public Feature.Color getColor();

}
~~~~

## Example

Here is a fully working example of a player who plays the first cards it has not played yet to the first available class and if none, create a new class:
~~~~
package fr.inria.moex.classapp;

import java.util.Properties;

public class MyClassPlayer extends AbstractClassPlayer implements ClassPlayer {

    public MyClassPlayer( ClassEnvironment e, int id ) {
        super( e, id );
        System.out.println( "Created player "+getId() );
    }

    // To records where cards have been played
    private CardSet[] cache;

    public MyClassPlayer init( Properties params ) {
        super.init( params );
        cache = new CardSet[CardSet.NBCARDS]; // creates a far too large cache
        System.out.println( "Initialised player "+getId() );
        return this;
    }

    @Override
    public void playGame( ClassGame game ) throws Exception {
        System.out.print( "Player "+getId()+" plays" );
        // Try something
        int nbClasses = getCurrentClassNumber();
        CardSet crds = new CardSet();
        for ( int cl=0; cl < nbClasses; cl++ ) { // Iterate on classes
            // It is possible to look into the class through
            // getCurrentClassCards( cl )
            // If cache not ready initialise it
            if ( cache[cl] == null ) cache[cl] = new CardSet();
            for ( Card crd: getHand() ) { // Iterate on cards in the hand
                if ( !cache[cl].includes( crd ) ) { // If not played in class
                    crds.addCard( crd ); // 
                    game.play( crds, cl ); // play this card to this class
                    cache[cl].addCard( crd );// cache it
                    System.out.println( " card "+printCard( crd )+" to class "+cl );
                    return; // we only play one card
                }
            }
        } // Apparently, all cards have been played in all classes
        for ( Card crd: getHand() ) {
            crds.addCard( crd ); // add the first card 
            game.play( crds, -1 ); // play it in a new class
            // it will be a success, we do not need to cache it
            System.out.println( " card "+printCard( crd )+" to new class " );
            return;
        }
    }

    // The issue of each move is notified
    @Override
    public void acceptNotice( ClassGame game ) {
        if ( game.getPlayerId() == getId() ) {
            if ( game.getIssue() ) {
                System.out.println( "This was correct" );
            } else {
                System.out.println( "This was not correct" );
            }
        }
    }

    @Override
    public void receiveCards( CardSet cards ) {
    super.receiveCards( cards ); // will put the received cards in the hand
        System.out.println( "Player "+getId()+" receiced cards: " );
        for ( Card crd : cards ) { // print each cards characteristics
            System.out.println( "  "+printCard( crd ) );
        }
    }

    @Override
    public void releaseCards( CardSet cards ) {
        super.releaseCards( cards ); // Will put the played cards out of the hand
    }

    private String printCard( Card crd ) {
        return crd.getNumber()+"-"+crd.getColor()+"-"+crd.getFilling()+"-"+crd.getShape();
    }
    
}
~~~~

## You can try it

The example belongs to the distribution so you can try it. 
Either in command line:
~~~~
$ java -jar lib/classapp/classapp.jar -Dplayer=fr.inria.moex.classapp.MyClassPlayer -DnbRuns=1 -DnbAgents=3 -DnbInitCards=1
~~~~
or graphically:
~~~~
$ java -Duser.language=fr -Dlog.level=INFO -jar lib/classapp/classgui.jar -Dplayer=fr.inria.moex.classapp.MyClassPlayer -DnbRuns=1 -DnbAgents=4 -DnbInitCards=2 -Dinteractive
~~~~
Here you play against them so it should be easy, right?

Notice that we have to launch the game with only three players receiving one card!
The reason is that this strategy is so bad that if they start with more cards, the knower will always win
(if the knower were playing like these agents, i.e. blindly, then the players will exhaust the deck with the penalties they receive).

Can you do better?

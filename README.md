# Classapp

An interactive application in Java to play the _Class?_ game.
It is currently working as a stand-alone application in command line on a Unix shell or through a simple graphic user interface.

The _Class?_ game is described at [https://moex.inria.fr/mediation/class](https://moex.inria.fr/mediation/class)

<center><img src="images/docs/gui.png" /></center>

The jarfile of the application can be obtained at [https://moex.inria.fr/files/software/classgui.jar](https://moex.inria.fr/files/software/classgui.jar).
It is self-contained and should run with Java starting with version 8.
In case Java is not available on your machine, you can install it from the <a href="https://adoptium.net/download/">Eclipse adoptium installers</a> (suggested install JRE 11 for your machine, as Java 8 does not load UTF-8 strings gracefully).

## Running

GUI:
~~~~
$ java -Dlog.level=NONE -cp logback.xml -jar lib/classapp/classgui.jar 
~~~~
or simply double-click the jar file.
Double-clicking or this line takes the default parameters for the players.
Hence, there will be two not very good players.
It is not yet possible to change these defaults from the GUI.

Instructions may be obtained by hiting keys (i)nfo, (h)elp, (q)uit and eventually others.
(s)ettings displays the settings but change them at the risk of a crash.

CLI (very abstractly rendered):
~~~~
$ java -Dlog.level=DEBUG -jar lib/classapp/classapp.jar -Dexperiment=fr.inria.moex.classapp.ClassSession -DnbRuns=1 -DnbAgents=6 -DclassStrategy=LOWRISK -DclassKnowerStrategy=LARGEST -Dinteractive -Dhints
~~~~
(please read the messages about the need for logger implementation --we advise logback)

In both cases the same arguments may be passed:
- nbAgents: the number of agents to play
- classStrategy: the strategies that guesser agents will follow (RANDOM,NORISK,BESTFITTINGCLASS,LITTLE,LOWRISK,CLASSIF,CLASSHIST)
- classKnowerStrategy: the strategy that the knower agent will follow (FIRST,FIRSTFULL,LARGEST,MININFO)
- interactive: if there is an interactive agent (that'll be you in command line)
- hints: present a synthetic view of the current state of the game

## Building

~~~~
$ ant compileall
~~~~

Requirements: java (Compiles in Java 8, but at least Java 9 is required to deal with UTF-8 characters in a native way), ant (for compiling), other libraries are included here.

## Programming interface

There is a [simple API](INTERFACE.md) with which you may implement your own strategies!


## [TODOs](TODO.md)
